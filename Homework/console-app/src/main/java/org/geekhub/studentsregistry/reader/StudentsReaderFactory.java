package org.geekhub.studentsregistry.reader;

import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class StudentsReaderFactory {
    private final ConsoleStudentsReader consoleStudentsReader;
    private final AutoStudentsGenerator autoStudentsGenerator;

    public StudentsReaderFactory(
        ConsoleStudentsReader consoleStudentsReader,
        AutoStudentsGenerator autoStudentsGenerator
    ) {
        this.consoleStudentsReader = consoleStudentsReader;
        this.autoStudentsGenerator = autoStudentsGenerator;
    }

    public StudentsReader createReader(InputMode inputMode) {
        if (Objects.isNull(inputMode)) {
            throw new IllegalArgumentException();
        }
        return switch (inputMode) {
            case AUTO -> autoStudentsGenerator;
            case MANUAL -> consoleStudentsReader;
        };
    }
}
