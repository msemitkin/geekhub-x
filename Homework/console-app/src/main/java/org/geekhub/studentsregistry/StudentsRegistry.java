package org.geekhub.studentsregistry;

import org.geekhub.studentsregistry.filterer.StudentsFilterer;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.mapper.StudentMapper;
import org.geekhub.studentsregistry.printer.ConsoleStudentsPrinter;
import org.geekhub.studentsregistry.reader.InputMode;
import org.geekhub.studentsregistry.reader.StudentsReader;
import org.geekhub.studentsregistry.reader.StudentsReaderFactory;
import org.geekhub.studentsregistry.sorter.StudentsSorter;
import org.geekhub.studentsregistry.storage.database.StudentEntity;
import org.geekhub.studentsregistry.storage.database.StudentRepository;
import org.geekhub.studentsregistry.student.Student;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StudentsRegistry {
    private final StudentsReaderFactory studentsReaderFactory;
    private final StudentsSorter studentsSorter;
    private final StudentsFilterer studentsFilterer;
    private final ConsoleStudentsPrinter consoleStudentsPrinter;
    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;

    public StudentsRegistry(
        StudentsReaderFactory studentsReaderFactory,
        StudentsSorter studentsSorter,
        StudentsFilterer studentsFilterer,
        ConsoleStudentsPrinter consoleStudentsPrinter,
        StudentRepository studentRepository,
        StudentMapper studentMapper
    ) {
        this.studentsReaderFactory = studentsReaderFactory;
        this.studentsSorter = studentsSorter;
        this.studentsFilterer = studentsFilterer;
        this.consoleStudentsPrinter = consoleStudentsPrinter;
        this.studentRepository = studentRepository;
        this.studentMapper = studentMapper;
    }

    private boolean isNotEmpty(List<Student> list) {
        return !list.isEmpty();
    }

    public void run(int totalStudentsCount, InputMode inputMode) {

        StudentsReader studentsReader = studentsReaderFactory.createReader(inputMode);
        List<Student> studentsFromConsole = studentsReader.readStudents(totalStudentsCount);

        List<StudentEntity> entities = studentsFromConsole.stream().map(studentMapper::toEntity).collect(Collectors.toList());
        studentRepository.saveAll(entities);
        List<Student> students = studentRepository.findAll().stream()
            .map(studentMapper::toStudent)
            .collect(Collectors.toList());

        Map<GradeType, List<Student>> filteredStudents =
            studentsFilterer.getStudentsFilteredByGradeType(students);

        for (Map.Entry<GradeType, List<Student>> entry : filteredStudents.entrySet()) {
            if (isNotEmpty(entry.getValue())) {
                List<Student> sortedStudents = studentsSorter.sortedStudents(entry.getValue());
                System.out.printf("Students with %s grade:%n", entry.getKey());
                consoleStudentsPrinter.printStudentsTable(sortedStudents);
                consoleStudentsPrinter.printStudentsAnalyticsInCategory(entry.getKey());
                System.out.println();
            }
        }
    }
}