package org.geekhub.studentsregistry.util;

import org.geekhub.studentsregistry.logger.Logging;
import org.geekhub.studentsregistry.reader.InputMode;
import org.geekhub.studentsregistry.reader.InputModeNotSupportedException;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class StartupArgumentsResolver {

    public int getStudentsCountFromArgs(String[] args) {
        int totalStudentsCount;
        if (args.length < 1) {
            Logging.getLogger().error("Total students count is expected as "
                                      + "a first start argument but missing");
            throw new InvalidStartupArgumentException();
        }
        try {
            totalStudentsCount = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            Logging.getLogger().error("Total students count is expected as "
                                      + "a first start argument but given: \"" + args[0] + "\"");
            throw new InvalidStartupArgumentException();
        }
        if (totalStudentsCount < 1) {
            Logging.getLogger().error("Total students count should be a positive integer number");
            throw new InvalidStartupArgumentException();
        }
        return totalStudentsCount;
    }

    public InputMode getInputModeFromArgs(String[] args) {
        if (args.length < 2) {
            Logging.getLogger().error(
                "Input mode is expected as a second start argument, but missing"
            );
            throw new InvalidStartupArgumentException();
        }
        try {
            return InputMode.from(args[1]);
        } catch (InputModeNotSupportedException e) {
            Logging.getLogger().error(
                String.format("Input mode is expected as a second start argument,"
                              + " but given: \"%s\". Available mods: %s%n",
                    args[1], Arrays.toString(InputMode.values()))
            );
            throw new InvalidStartupArgumentException();
        }
    }
}
