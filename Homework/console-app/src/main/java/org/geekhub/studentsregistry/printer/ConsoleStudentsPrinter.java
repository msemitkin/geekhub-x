package org.geekhub.studentsregistry.printer;

import org.geekhub.studentsregistry.analyst.StudentsAnalyst;
import org.geekhub.studentsregistry.grade.CommonGradeFactory;
import org.geekhub.studentsregistry.grade.Grade;
import org.geekhub.studentsregistry.grade.GradeFactory;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.student.Student;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ConsoleStudentsPrinter implements StudentsPrinter {
    private final StudentsAnalyst studentsAnalyst;

    public ConsoleStudentsPrinter(StudentsAnalyst studentsAnalyst) {
        this.studentsAnalyst = studentsAnalyst;
    }

    public void printStudentsTable(List<Student> students) {
        if (students.isEmpty()) {
            return;
        }
        System.out.println("+-------------------------------+---------+------------+-----+");
        System.out.println("|Name                           |Grade    |Date of exam|Time |");
        System.out.println("+-------------------------------+---------+------------+-----+");
        for (Student student : students) {
            System.out.printf("|%-31s|%-9s|%-12s|%-5s|%n", student.name(),
                student.grade().asPrintVersion(), student.examDate(),
                student.examTime().truncatedTo(ChronoUnit.MINUTES));
        }
        System.out.println("+-------------------------------+---------+------------+-----+");
    }

    public void printStudentsAnalyticsInCategory(GradeType gradeType) {
        printMaxScore(gradeType);
        printMinScore(gradeType);
        printMedianScore(gradeType);
    }

    private String inAllGrades(int score) {
        GradeFactory gradeFactory = new CommonGradeFactory();
        return Stream.of(GradeType.values())
            .map(gradeType -> gradeFactory.createGrade(gradeType, score))
            .map(Grade::asPrintVersion)
            .collect(Collectors.joining(", ", "[", "]"));
    }

    private void printMaxScore(GradeType gradeType) {
        String maxScoreRepresentation = studentsAnalyst.maxScore(gradeType)
            .map(this::inAllGrades)
            .orElse("N/A");
        System.out.printf("Max score in %s category is: %s%n",
            gradeType.name(), maxScoreRepresentation);
    }

    private void printMinScore(GradeType gradeType) {
        String minScoreRepresentation = studentsAnalyst.minScore(gradeType)
            .map(this::inAllGrades)
            .orElse("N/A");
        System.out.printf("Min score in %s category is: %s%n",
            gradeType.name(), minScoreRepresentation);
    }

    private void printMedianScore(GradeType gradeType) {
        String medianScoreRepresentation = studentsAnalyst.medianScore(gradeType)
            .map(this::inAllGrades)
            .orElse("N/A");
        System.out.printf("Median score in %s category is: %s%n",
            gradeType.name(), medianScoreRepresentation);
    }
}
