package org.geekhub.studentsregistry;

import org.geekhub.studentsregistry.reader.InputMode;
import org.geekhub.studentsregistry.util.StartupArgumentsResolver;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsoleApplication implements CommandLineRunner {

    private final StudentsRegistry studentsRegistry;
    private final StartupArgumentsResolver startupArgumentsResolver;

    public ConsoleApplication(
        StudentsRegistry studentsRegistry,
        StartupArgumentsResolver startupArgumentsResolver
    ) {
        this.studentsRegistry = studentsRegistry;
        this.startupArgumentsResolver = startupArgumentsResolver;
    }

    public static void main(String[] args) {
        SpringApplication.run(ConsoleApplication.class, args);
    }

    @Override
    public void run(String... args) {
        int totalStudentsCount = startupArgumentsResolver.getStudentsCountFromArgs(args);
        InputMode inputMode = startupArgumentsResolver.getInputModeFromArgs(args);
        System.out.printf("Input mode: %s. Students count: %d%n",
            inputMode.name(), totalStudentsCount);
        studentsRegistry.run(totalStudentsCount, inputMode);
    }
}