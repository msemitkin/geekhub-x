package org.geekhub.studentsregistry.reader;

import org.geekhub.studentsregistry.InvalidInputException;
import org.geekhub.studentsregistry.grade.CommonGradeFactory;
import org.geekhub.studentsregistry.grade.GradeAdapter;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.student.Student;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Component
public class ConsoleStudentsReader implements StudentsReader {

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String TIME_FORMAT = "HH:mm";
    Scanner scanner;

    public ConsoleStudentsReader(Scanner scanner) {
        this.scanner = scanner;
    }


    private Optional<String> readStudentName() {
        System.out.println("Name:");
        Optional<String> studentNameOpt = readNotBlankOpt();
        if (studentNameOpt.isEmpty()) {
            System.out.println("Student name must be not empty");
        }
        return studentNameOpt;
    }

    private Optional<String> readNotBlankOpt() {
        return Optional.of(scanner.nextLine().trim())
            .filter(name -> !name.isBlank());
    }

    private Optional<Integer> readStudentGrade() {
        System.out.println("Grade [0; 100]:");
        Optional<Integer> score = Optional.empty();
        Optional<String> input = readNotBlankOpt();
        if (input.isEmpty()) {
            System.out.println("Student grade was not given");
            return Optional.empty();
        }
        try {
            score = input.map(Integer::parseInt).filter(grade -> 0 <= grade && grade <= 100);
            if (score.isEmpty()) {
                System.out.println("Entered score value is not in an allowed range [0; 100]");
            }
        } catch (NumberFormatException e) {
            System.out.println("Grade must consist of integers");
        }
        return score;
    }


    private Optional<GradeType> readGradeType() {
        System.out.println("Grade type:");
        Optional<String> gradeType = readNotBlankOpt();
        if (gradeType.isEmpty()) {
            System.out.println("Grade type was not given");
        }
        return gradeType.map(GradeType::from);
    }


    private Optional<LocalDate> readExamDate() {
        System.out.printf("Date of exam [%s]:%n", DATE_FORMAT);
        Optional<String> date = readNotBlankOpt();
        if (date.isEmpty()) {
            System.out.println("Date of exam was not given");
            return Optional.empty();
        }
        try {
            return date.map(LocalDate::parse);
        } catch (DateTimeParseException e) {
            System.out.println("Given date is not valid");
        }
        return Optional.empty();
    }

    private Optional<LocalTime> readExamTime() {
        System.out.printf("Time of exam [%s]:%n", TIME_FORMAT);
        Optional<String> time = readNotBlankOpt();
        if (time.isEmpty()) {
            System.out.println("Time of exam was not given");
            return Optional.empty();
        }
        try {
            return time.map(LocalTime::parse);
        } catch (DateTimeParseException e) {
            System.out.println("Given time is not valid");
        }
        return Optional.empty();
    }

    private Student readSingleStudent() {
        Optional<String> nameOpt = Optional.empty();
        while (nameOpt.isEmpty()) {
            nameOpt = readStudentName();
        }
        Optional<Integer> intGradeOpt = Optional.empty();
        while (intGradeOpt.isEmpty()) {
            intGradeOpt = readStudentGrade();
        }
        Optional<GradeType> gradeTypeOpt = Optional.empty();
        while (gradeTypeOpt.isEmpty()) {
            gradeTypeOpt = readGradeType();
        }
        Optional<LocalDate> examDateOpt = Optional.empty();
        while (examDateOpt.isEmpty()) {
            examDateOpt = readExamDate();
        }
        Optional<LocalTime> examTimeOpt = Optional.empty();
        while (examTimeOpt.isEmpty()) {
            examTimeOpt = readExamTime();
        }
        CommonGradeFactory gradeFactory = new CommonGradeFactory();
        GradeAdapter grade = gradeFactory.createGrade(gradeTypeOpt.get(), intGradeOpt.get());
        return new Student(nameOpt.get(), grade, examDateOpt.get(), examTimeOpt.get());
    }

    public List<Student> readStudents(int totalStudentsCount) {
        if (totalStudentsCount < 1) {
            System.out.println("Total student count must be a positive number");
            throw new IllegalArgumentException();
        }
        System.out.printf("Group consists of %d students%n", totalStudentsCount);
        List<Student> inputStudents = new ArrayList<>();
        int i = 0;
        while (i < totalStudentsCount) {
            try {
                inputStudents.add(readSingleStudent());
            } catch (InvalidInputException e) {
                continue;
            }
            i++;
        }
        System.out.println("Students info have been successfully read");
        return inputStudents;
    }

}