package org.geekhub.studentsregistry.reader;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Random;
import java.util.Scanner;

public class StudentsReaderFactoryTest {
    StudentsReaderFactory studentsReaderFactory;

    @BeforeMethod
    public void setUp() {
        studentsReaderFactory = new StudentsReaderFactory(
            new ConsoleStudentsReader(new Scanner("")),
            new AutoStudentsGenerator(new Random())
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void cant_create_reader_for_null_input() {
        studentsReaderFactory.createReader(null);
    }

    @Test
    public void autoStudentsGenerator_is_created_for_AUTO_mode() {
        StudentsReader studentsReader = studentsReaderFactory.createReader(InputMode.AUTO);
        Assert.assertTrue(studentsReader instanceof AutoStudentsGenerator);
    }

    @Test
    public void consoleStudentsReader_is_created_for_MANUAL_mode() {
        StudentsReader studentsReader = studentsReaderFactory.createReader(InputMode.MANUAL);
        Assert.assertTrue(studentsReader instanceof ConsoleStudentsReader);
    }
}