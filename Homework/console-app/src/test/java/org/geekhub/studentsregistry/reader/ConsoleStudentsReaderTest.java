package org.geekhub.studentsregistry.reader;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.Scanner;

public class ConsoleStudentsReaderTest {
    ConsoleStudentsReader consoleStudentsReader;

    @BeforeMethod
    private void setUp() {
        consoleStudentsReader = new ConsoleStudentsReader(new Scanner(System.in));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void readStudents_throws_IllegalArgumentException_when_totalStudentCount_is_not_a_positive_number() {
        consoleStudentsReader.readStudents(0);
    }

}