package org.geekhub.studentsregistry;

import org.assertj.core.api.Assertions;
import org.geekhub.studentsregistry.filterer.StudentsFilterer;
import org.geekhub.studentsregistry.grade.GradeTestData;
import org.geekhub.studentsregistry.mapper.StudentMapper;
import org.geekhub.studentsregistry.printer.ConsoleStudentsPrinter;
import org.geekhub.studentsregistry.reader.ConsoleStudentsReader;
import org.geekhub.studentsregistry.reader.InputMode;
import org.geekhub.studentsregistry.reader.StudentsReaderFactory;
import org.geekhub.studentsregistry.sorter.StudentsSorter;
import org.geekhub.studentsregistry.storage.database.StudentRepository;
import org.geekhub.studentsregistry.student.Student;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.geekhub.studentsregistry.DateTimeData.DATE;
import static org.geekhub.studentsregistry.DateTimeData.TIME;

public class StudentsRegistryTest {
    @Mock
    private StudentsReaderFactory studentsReaderFactory;
    @Mock
    private ConsoleStudentsReader consoleStudentsReader;
    @Mock
    private ConsoleStudentsPrinter consoleStudentsPrinter;
    @Mock
    private StudentRepository studentRepository;
    @Mock
    private StudentsSorter studentsSorter;
    @Mock
    private StudentsFilterer studentsFilterer;
    @Mock
    private StudentMapper studentMapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void application_flow_is_completed_successfully() {
        Student student = new Student("name", GradeTestData.LETTER_GRADE, DATE, TIME);
        List<Student> students = List.of(student);

        int totalStudentsCount = 1;
        InputMode inputMode = InputMode.MANUAL;

        StudentsRegistry studentsRegistry = new StudentsRegistry(
            studentsReaderFactory,
            studentsSorter,
            studentsFilterer,
            consoleStudentsPrinter,
            studentRepository,
            studentMapper
        );

        Mockito.when(studentsSorter.sortedStudents(Mockito.anyList()))
            .thenReturn(Collections.emptyList());
        Mockito.when(studentsFilterer.getStudentsFilteredByGradeType(Mockito.anyList()))
            .thenReturn(Collections.emptyMap());
        Mockito.when(studentsReaderFactory.createReader(inputMode))
            .thenReturn(consoleStudentsReader);
        Mockito.when(studentRepository.findAll())
            .thenReturn(Collections.emptyList());
        Mockito.when(consoleStudentsReader.readStudents(totalStudentsCount))
            .thenReturn(students);

        Assertions.assertThatCode(
            () -> studentsRegistry.run(totalStudentsCount, inputMode)
        ).doesNotThrowAnyException();

    }
}