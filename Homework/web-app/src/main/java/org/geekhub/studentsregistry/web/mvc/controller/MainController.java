package org.geekhub.studentsregistry.web.mvc.controller;

import org.geekhub.studentsregistry.web.common.dto.StudentCountDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping({"", "main-menu", "/"})
    public String root(Model model) {
        model.addAttribute("count", new StudentCountDTO());
        return "main-menu";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

}