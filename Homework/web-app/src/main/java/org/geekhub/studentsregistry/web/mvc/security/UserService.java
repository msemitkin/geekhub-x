package org.geekhub.studentsregistry.web.mvc.security;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class UserService {

    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;

    public UserService(UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    public void createUser(UserDetails user) {
        Assert.hasText(user.getUsername(), "Username must be not empty");
        Assert.hasText(user.getPassword(), "Password must be not empty");

        if (userDetailsManager.userExists(user.getUsername())) {
            throw new UserRegistrationException("User with given username already exists");
        }

        String encodedPassword = passwordEncoder.encode(user.getPassword());
        UserDetails userDetails =
            User.withUserDetails(user)
                .password(encodedPassword)
                .build();
        userDetailsManager.createUser(userDetails);
    }

    public void changePassword(String oldPassword, String newPassword) {
        String newEncodedPassword = passwordEncoder.encode(newPassword);
        userDetailsManager.changePassword(oldPassword, newEncodedPassword);
    }

    public void deleteUser(String username) {
        Assert.hasText(username, "Username must be not empty");

        if (userDetailsManager.userExists(username)) {
            userDetailsManager.deleteUser(username);
        } else {
            throw new UsernameNotFoundException("User with given username does not exist");
        }
    }
}
