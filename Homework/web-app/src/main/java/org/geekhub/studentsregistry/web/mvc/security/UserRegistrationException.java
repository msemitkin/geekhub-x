package org.geekhub.studentsregistry.web.mvc.security;

public class UserRegistrationException extends RuntimeException {
    public UserRegistrationException(String s) {
        super(s);
    }
}
