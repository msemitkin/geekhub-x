package org.geekhub.studentsregistry.web.mvc.security.config;

import org.geekhub.studentsregistry.storage.database.DatabaseQueries;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

import javax.sql.DataSource;

@Configuration
public class UserDetailsManagerConfig {

    private final DatabaseQueries queries;

    public UserDetailsManagerConfig(DatabaseQueries queries) {
        this.queries = queries;
    }

    @Bean
    public JdbcUserDetailsManager userDetailsManager(
        AuthenticationManager authenticationManager,
        AuthenticationManagerBuilder auth,
        @Qualifier("dataSource") DataSource dataSource,
        PasswordEncoder passwordEncoder
    ) throws Exception {

        JdbcUserDetailsManager userDetailsManager =
            auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder)
                .usersByUsernameQuery(queries.getQuery("get-user-by-username.sql"))
                .authoritiesByUsernameQuery(queries.getQuery("get-authority-by-username.sql"))
                .getUserDetailsService();

        userDetailsManager.setAuthenticationManager(authenticationManager);
        userDetailsManager.setUserExistsSql(queries.getQuery("user-exists.sql"));
        userDetailsManager.setCreateUserSql(queries.getQuery("create-user.sql"));
        userDetailsManager.setCreateAuthoritySql(queries.getQuery("create-authority.sql"));
        userDetailsManager.setDeleteUserSql(queries.getQuery("delete-user.sql"));
        userDetailsManager.setDeleteUserAuthoritiesSql(queries.getQuery("delete-authority.sql"));
        userDetailsManager.setChangePasswordSql(queries.getQuery("change-password.sql"));

        return userDetailsManager;

    }

}
