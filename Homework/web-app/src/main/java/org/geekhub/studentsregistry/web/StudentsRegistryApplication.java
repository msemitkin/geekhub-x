package org.geekhub.studentsregistry.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentsRegistryApplication {
    public static void main(String[] args) {
        SpringApplication.run(StudentsRegistryApplication.class, args);
    }
}