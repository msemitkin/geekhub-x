package org.geekhub.studentsregistry.web.common.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class StudentCountDTO {

    @NotNull(message = "Field is required")
    @Min(value = 1, message = "Must be a positive number")
    private Integer count;

    public StudentCountDTO() {
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentCountDTO that = (StudentCountDTO) o;
        return Objects.equals(count, that.count);
    }

    @Override
    public int hashCode() {
        return Objects.hash(count);
    }

    @Override
    public String toString() {
        return "StudentCountDTO{" +
               "count=" + count +
               '}';
    }
}
