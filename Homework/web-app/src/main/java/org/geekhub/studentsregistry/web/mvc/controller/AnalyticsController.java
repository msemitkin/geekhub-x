package org.geekhub.studentsregistry.web.mvc.controller;

import org.geekhub.studentsregistry.web.common.service.StudentAnalyticsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AnalyticsController {

    private final StudentAnalyticsService studentAnalyticsService;

    public AnalyticsController(StudentAnalyticsService studentAnalyticsService) {
        this.studentAnalyticsService = studentAnalyticsService;
    }

    @GetMapping("/students/analytics")
    public String analytics(Model model) {
        model.addAttribute("analytics", studentAnalyticsService.analyticsInEachCategory());
        return "analytics";
    }

}
