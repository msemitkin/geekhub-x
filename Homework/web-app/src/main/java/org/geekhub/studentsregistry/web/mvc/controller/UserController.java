package org.geekhub.studentsregistry.web.mvc.controller;

import org.geekhub.studentsregistry.web.common.dto.CreateUserDTO;
import org.geekhub.studentsregistry.web.mvc.security.UserService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class UserController {

    private static final String REDIRECT_TO_ROOT = "redirect:/";

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users/me")
    public String getMe(
        Model model,
        @AuthenticationPrincipal UserDetails userDetails
    ) {
        model.addAttribute("user", userDetails);
        return "user";
    }

    @GetMapping("/register")
    public String addUser(Model model) {
        model.addAttribute("user", new CreateUserDTO());
        return "user-form";
    }

    @PostMapping("/users/me")
    public String changePassword(
        Model model,
        @AuthenticationPrincipal UserDetails user,
        @RequestParam("old-password") String oldPassword,
        @RequestParam("new-password") String newPassword
    ) {
        userService.changePassword(oldPassword, newPassword);
        model.addAttribute("user", user);
        return REDIRECT_TO_ROOT;
    }

    @PostMapping("/users/me/delete")
    public String deleteMe(@AuthenticationPrincipal UserDetails userDetails) {

        userService.deleteUser(userDetails.getUsername());
        SecurityContextHolder.clearContext();

        return REDIRECT_TO_ROOT;
    }

    @PostMapping("/users")
    public String createUser(
        @Valid @ModelAttribute("user") CreateUserDTO userDTO,
        BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            return "user-form";
        }
        UserDetails user = User.builder()
            .password(userDTO.getPassword())
            .username(userDTO.getUsername())
            .disabled(false)
            .authorities("USER")
            .build();
        userService.createUser(user);
        return REDIRECT_TO_ROOT;
    }
}
