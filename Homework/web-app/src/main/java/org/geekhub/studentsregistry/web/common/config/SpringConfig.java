package org.geekhub.studentsregistry.web.common.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.geekhub.studentsregistry")
public class SpringConfig {
}
