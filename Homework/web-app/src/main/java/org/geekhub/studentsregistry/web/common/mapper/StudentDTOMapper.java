package org.geekhub.studentsregistry.web.common.mapper;

import org.geekhub.studentsregistry.grade.CommonGradeFactory;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.student.Student;
import org.geekhub.studentsregistry.web.common.dto.CreateStudentDTO;
import org.geekhub.studentsregistry.web.common.dto.StudentDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;

@Component
public class StudentDTOMapper {

    private final CommonGradeFactory gradeFactory;

    public StudentDTOMapper(CommonGradeFactory gradeFactory) {
        this.gradeFactory = gradeFactory;
    }

    public StudentDto toDTO(Student student) {
        return new StudentDto(
            student.name(),
            student.grade().asPrintVersion(),
            student.examDate().toString(),
            student.examTime().toString()
        );
    }

    public Student toStudent(CreateStudentDTO studentDTO) {
        GradeType gradeType = GradeType.from(studentDTO.getGradeType());
        return new Student(
            studentDTO.getName(),
            gradeFactory.createGrade(gradeType, studentDTO.getScore()),
            LocalDate.parse(studentDTO.getDate()),
            LocalTime.parse(studentDTO.getTime())
        );
    }

}
