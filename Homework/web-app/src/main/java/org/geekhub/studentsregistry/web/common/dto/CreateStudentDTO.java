package org.geekhub.studentsregistry.web.common.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class CreateStudentDTO {
    @NotNull
    @NotBlank(message = "Name must be not blank")
    private String name;
    @NotNull(message = "Score is required")
    @Min(value = 0, message = "Score must be positive")
    @Max(value = 100, message = "Score must be less or equal {value}")
    private Integer score;
    @NotBlank(message = "Grade type is required")
    private String gradeType;
    @NotBlank(message = "Date is required")
    private String date;
    @NotBlank(message = "Time is required")
    private String time;

    public CreateStudentDTO() {
    }

    public CreateStudentDTO(
        String name,
        Integer score,
        String gradeType,
        String date,
        String time
    ) {
        this.name = name;
        this.score = score;
        this.gradeType = gradeType;
        this.date = date;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getGradeType() {
        return gradeType;
    }

    public void setGradeType(String gradeType) {
        this.gradeType = gradeType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateStudentDTO that = (CreateStudentDTO) o;
        return Objects.equals(name, that.name) &&
               Objects.equals(score, that.score) &&
               Objects.equals(gradeType, that.gradeType) &&
               Objects.equals(date, that.date) &&
               Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, score, gradeType, date, time);
    }

    @Override
    public String toString() {
        return "CreateStudentDTO{" +
               "name='" + name + '\'' +
               ", score=" + score +
               ", gradeType='" + gradeType + '\'' +
               ", date='" + date + '\'' +
               ", time='" + time + '\'' +
               '}';
    }
}
