package org.geekhub.studentsregistry.web.common.dto;

import javax.validation.constraints.Size;
import java.util.Objects;

public class CreateUserDTO {
    @Size(min = 6, max = 32,
        message = "Username name length must be between {min} and {max} characters")
    private String username;
    @Size(min = 6, message = "Password must be at least {min} characters")
    private String password;

    public CreateUserDTO() {
    }

    public CreateUserDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateUserDTO that = (CreateUserDTO) o;
        return Objects.equals(username, that.username) && Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }

    @Override
    public String toString() {
        return "CreateUserDTO{" +
               "username='" + username + '\'' +
               ", password='" + password + '\'' +
               '}';
    }
}
