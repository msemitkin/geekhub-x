package org.geekhub.studentsregistry.web.common.service;

import org.geekhub.studentsregistry.analyst.Analytics;
import org.geekhub.studentsregistry.analyst.CachingStudentAnalyticsManager;
import org.geekhub.studentsregistry.grade.CommonGradeFactory;
import org.geekhub.studentsregistry.grade.Grade;
import org.geekhub.studentsregistry.grade.GradeFactory;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.web.common.dto.AnalyticsDTO;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class StudentAnalyticsService {

    private final CachingStudentAnalyticsManager analyticsManager;

    public StudentAnalyticsService(CachingStudentAnalyticsManager analyticsManager) {
        this.analyticsManager = analyticsManager;
    }

    public Map<String, AnalyticsDTO> analyticsInEachCategory() {
        return analyticsManager.getAnalytics().entrySet()
            .stream()
            .collect(Collectors.toMap(
                entry -> entry.getKey().name(),
                entry -> toDTO(entry.getValue())
                )
            );
    }

    private String inAllGrades(int score) {
        GradeFactory gradeFactory = new CommonGradeFactory();
        return Stream.of(GradeType.values())
            .map(gradeType -> gradeFactory.createGrade(gradeType, score))
            .map(Grade::asPrintVersion)
            .collect(Collectors.joining(", ", "[", "]"));
    }

    private String getRepresentation(Optional<Integer> score) {
        return score
            .map(this::inAllGrades)
            .orElse("N/A");
    }

    private AnalyticsDTO toDTO(Analytics analytics) {
        return new AnalyticsDTO(
            getRepresentation(analytics.getMax()),
            getRepresentation(analytics.getMin()),
            getRepresentation(analytics.getMedian())
        );
    }
}
