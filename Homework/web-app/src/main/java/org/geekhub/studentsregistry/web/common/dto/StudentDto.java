package org.geekhub.studentsregistry.web.common.dto;

import java.util.Objects;

public class StudentDto {

    private final String name;
    private final String gradeRepresentation;
    private final String examDate;
    private final String examTime;

    public StudentDto(
        String name,
        String gradeRepresentation,
        String examDate,
        String examTime
    ) {
        this.name = name;
        this.gradeRepresentation = gradeRepresentation;
        this.examDate = examDate;
        this.examTime = examTime;
    }

    public String getName() {
        return name;
    }

    public String getGradeRepresentation() {
        return gradeRepresentation;
    }

    public String getExamDate() {
        return examDate;
    }

    public String getExamTime() {
        return examTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentDto that = (StudentDto) o;
        return Objects.equals(name, that.name) && Objects.equals(gradeRepresentation, that.gradeRepresentation) && Objects.equals(examDate, that.examDate) && Objects.equals(examTime, that.examTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, gradeRepresentation, examDate, examTime);
    }

    @Override
    public String toString() {
        return "StudentDto{" +
               "name='" + name + '\'' +
               ", gradeRepresentation='" + gradeRepresentation + '\'' +
               ", examDate='" + examDate + '\'' +
               ", examTime='" + examTime + '\'' +
               '}';
    }
}
