package org.geekhub.studentsregistry.web.api.controller;

import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.student.Student;
import org.geekhub.studentsregistry.web.common.dto.CreateStudentDTO;
import org.geekhub.studentsregistry.web.common.dto.StudentCountDTO;
import org.geekhub.studentsregistry.web.common.dto.StudentDto;
import org.geekhub.studentsregistry.web.common.mapper.StudentDTOMapper;
import org.geekhub.studentsregistry.web.common.service.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class StudentRestController {

    private final StudentService studentService;
    private final StudentDTOMapper studentDTOMapper;

    public StudentRestController(StudentService studentService, StudentDTOMapper studentDTOMapper) {
        this.studentService = studentService;
        this.studentDTOMapper = studentDTOMapper;
    }

    @GetMapping("api/students")
    public Map<GradeType, List<StudentDto>> getAll() {
        Map<GradeType, List<Student>> filteredStudents = studentService.getStudentsFilteredByGradeType();
        return filteredStudents.entrySet().stream()
            .collect(
                Collectors.toMap(
                    Map.Entry::getKey,
                    e -> e.getValue().stream()
                        .map(studentDTOMapper::toDTO)
                        .collect(Collectors.toList())
                )
            );
    }

    @PostMapping("api/students")
    @ResponseStatus(HttpStatus.CREATED)
    public void save(@Valid CreateStudentDTO studentDTO) {
        studentService.createStudent(studentDTOMapper.toStudent(studentDTO));
    }

    @DeleteMapping("/api/students/{id}")
    public void deleteById(@PathVariable("id") int id) {
        studentService.deleteById(id);
    }

    @DeleteMapping("api/students")
    public void deleteAll() {
        studentService.deleteAllStudents();
    }

    @PostMapping("api/students/generator")
    @ResponseStatus(HttpStatus.CREATED)
    public void generateStudents(@Valid StudentCountDTO countDTO) {
        studentService.generateStudents(countDTO.getCount());
    }
}
