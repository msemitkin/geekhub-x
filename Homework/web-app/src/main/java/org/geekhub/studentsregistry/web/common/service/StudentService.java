package org.geekhub.studentsregistry.web.common.service;

import org.geekhub.studentsregistry.filterer.StudentsFilterer;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.mapper.StudentMapper;
import org.geekhub.studentsregistry.reader.AutoStudentsGenerator;
import org.geekhub.studentsregistry.sorter.StudentsSorter;
import org.geekhub.studentsregistry.storage.StudentNotFoundException;
import org.geekhub.studentsregistry.storage.StudentsStorageManager;
import org.geekhub.studentsregistry.storage.database.StudentEntity;
import org.geekhub.studentsregistry.student.Student;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class StudentService {
    private final StudentsStorageManager studentsStorageManager;
    private final StudentsFilterer filterer;
    private final StudentsSorter sorter;
    private final AutoStudentsGenerator generator;
    private final StudentMapper studentMapper;

    public StudentService(
        StudentsStorageManager studentsStorageManager,
        StudentsFilterer filterer,
        StudentsSorter sorter,
        AutoStudentsGenerator generator,
        StudentMapper studentMapper
    ) {
        this.studentsStorageManager = studentsStorageManager;
        this.filterer = filterer;
        this.sorter = sorter;
        this.generator = generator;
        this.studentMapper = studentMapper;
    }

    public void generateStudents(int count) {
        List<Student> students = generator.readStudents(count);
        List<StudentEntity> entities = students.stream()
            .map(studentMapper::toEntity)
            .collect(Collectors.toList());
        studentsStorageManager.writeStudents(entities);
    }

    public void createStudent(Student student) {

        StudentEntity studentEntity = studentMapper.toEntity(student);
        studentsStorageManager.writeStudents(List.of(studentEntity));
    }

    public Map<GradeType, List<Student>> getStudentsFilteredByGradeType() {
        List<StudentEntity> studentEntities = studentsStorageManager.readStudents();
        if (studentEntities.isEmpty()) {
            return Collections.emptyMap();
        }

        List<Student> students = studentEntities.stream()
            .map(studentMapper::toStudent)
            .collect(Collectors.toList());

        Map<GradeType, List<Student>> sortedStudents = new EnumMap<>(GradeType.class);
        for (
            Map.Entry<GradeType, List<Student>> entry :
            filterer.getStudentsFilteredByGradeType(students).entrySet()
        ) {
            sortedStudents.put(entry.getKey(), sorter.sortedStudents(entry.getValue()));
        }

        return sortedStudents;
    }

    public void deleteById(int id) {
        if (studentsStorageManager.existsById(id)) {
            studentsStorageManager.deleteById(id);
        } else {
            throw new StudentNotFoundException();
        }
    }

    public void deleteAllStudents() {
        studentsStorageManager.deleteAll();
    }

}
