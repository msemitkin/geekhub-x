package org.geekhub.studentsregistry.web.api.controller;

import org.geekhub.studentsregistry.web.common.dto.AnalyticsDTO;
import org.geekhub.studentsregistry.web.common.service.StudentAnalyticsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class AnalyticsRestController {

    private final StudentAnalyticsService studentAnalyticsService;

    public AnalyticsRestController(StudentAnalyticsService studentAnalyticsService) {
        this.studentAnalyticsService = studentAnalyticsService;
    }

    @GetMapping("api/analytics")
    public Map<String, AnalyticsDTO> getAnalytics() {
        return studentAnalyticsService.analyticsInEachCategory();
    }

}
