package org.geekhub.studentsregistry.web.mvc.controller;

import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.student.Student;
import org.geekhub.studentsregistry.web.common.dto.CreateStudentDTO;
import org.geekhub.studentsregistry.web.common.dto.StudentCountDTO;
import org.geekhub.studentsregistry.web.common.dto.StudentDto;
import org.geekhub.studentsregistry.web.common.mapper.StudentDTOMapper;
import org.geekhub.studentsregistry.web.common.service.StudentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class StudentController {
    private static final String REDIRECT_STUDENTS_ALL = "redirect:/students/all";
    private final StudentService studentService;
    private final StudentDTOMapper studentDTOMapper;

    public StudentController(
        StudentService studentService,
        StudentDTOMapper studentDTOMapper
    ) {
        this.studentService = studentService;
        this.studentDTOMapper = studentDTOMapper;
    }

    @GetMapping("/students/all")
    public String getStudents(Model model) {
        Map<GradeType, List<Student>> studentsFilteredByGradeType = studentService.getStudentsFilteredByGradeType();
        Map<GradeType, List<StudentDto>> studentDTOs =
            studentsFilteredByGradeType.entrySet().stream()
                .collect(
                    Collectors.toMap(
                        Map.Entry::getKey,
                        e -> e.getValue().stream()
                            .map(studentDTOMapper::toDTO)
                            .collect(Collectors.toList())
                    )
                );
        model.addAttribute("students", studentDTOs);
        return "students";
    }

    @GetMapping("/students/new/manual")
    public String newStudents(Model model) {
        model.addAttribute("student", new CreateStudentDTO());
        return "student-form";
    }

    @PostMapping("/students/new/auto")
    public String generate(
        @Valid @ModelAttribute("count") StudentCountDTO countDTO,
        BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            return "main-menu";
        }
        studentService.generateStudents(countDTO.getCount());
        return REDIRECT_STUDENTS_ALL;
    }

    @PostMapping("/students/new")
    public String newStudent(
        @Valid @ModelAttribute("student") CreateStudentDTO studentDTO,
        BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            return "student-form";
        }
        studentService.createStudent(studentDTOMapper.toStudent(studentDTO));
        return REDIRECT_STUDENTS_ALL;
    }

    @PostMapping("/students/delete/all")
    public String delete() {
        studentService.deleteAllStudents();
        return REDIRECT_STUDENTS_ALL;
    }
}
