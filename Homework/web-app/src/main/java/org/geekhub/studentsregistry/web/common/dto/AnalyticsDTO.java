package org.geekhub.studentsregistry.web.common.dto;

import java.util.Objects;

public class AnalyticsDTO {
    private final String max;
    private final String min;
    private final String median;

    public AnalyticsDTO(String max, String min, String median) {
        this.max = max;
        this.min = min;
        this.median = median;
    }

    public String getMax() {
        return max;
    }

    public String getMin() {
        return min;
    }

    public String getMedian() {
        return median;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnalyticsDTO that = (AnalyticsDTO) o;
        return Objects.equals(max, that.max) &&
               Objects.equals(min, that.min) &&
               Objects.equals(median, that.median);
    }

    @Override
    public int hashCode() {
        return Objects.hash(max, min, median);
    }
}
