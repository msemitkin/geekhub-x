package org.geekhub.studentsregistry.web.common.errorhandling;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.geekhub.studentsregistry.logger.Logger;
import org.geekhub.studentsregistry.logger.Logging;
import org.geekhub.studentsregistry.storage.StudentNotFoundException;
import org.geekhub.studentsregistry.web.mvc.errorhandling.ErrorDTO;
import org.geekhub.studentsregistry.web.mvc.security.UserRegistrationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DefaultExceptionHandler {
    private static final Logger logger = Logging.getLogger();

    @ExceptionHandler(StudentNotFoundException.class)
    public ResponseEntity<String> studentNotFound(StudentNotFoundException e) {
        logger.error("Students with given id was not found " + ExceptionUtils.getStackTrace(e));
        return new ResponseEntity<>("Student not found", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UserRegistrationException.class)
    public String registrationException(Model model) {
        model.addAttribute("error", new ErrorDTO("User with this login already exists"));
        return "user-form";
    }

    @ExceptionHandler(BadCredentialsException.class)
    public String badCredentialsMessage(Model model) {
        model.addAttribute("error", new ErrorDTO("Bad credentials"));
        return "user";
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> internalServerError(Throwable e) {
        logger.error(ExceptionUtils.getStackTrace(e));
        return new ResponseEntity<>("Unknown error", HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
