create table user_credentials
(
    username varchar not null primary key,
    password varchar not null,
    enabled  boolean default true
);