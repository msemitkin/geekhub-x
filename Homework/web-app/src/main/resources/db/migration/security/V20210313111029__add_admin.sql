insert into user_credentials (username, password)
values ('admin', '$2y$12$NKjZ5nG2EPRwe5eEaGJn7.PjAQnFgYhoV5etbuS4lW1kMkN61v6tm');

insert into authority (username, authority)
values ('admin', 'ROLE_ADMIN')