create table authority
(
    id        serial not null primary key,
    username  varchar,
    authority varchar,
    constraint username_fkey foreign key (username) references user_credentials (username)
);