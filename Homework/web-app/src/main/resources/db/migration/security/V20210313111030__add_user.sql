insert into user_credentials (username, password)
values ('user', '$2y$12$fVkGb1xjtZA2yHhH/PJvI.czIJM9CcxLI8o2rU9Uz2HiOFxH3P/We');

insert into authority (username, authority)
values ('user', 'default')