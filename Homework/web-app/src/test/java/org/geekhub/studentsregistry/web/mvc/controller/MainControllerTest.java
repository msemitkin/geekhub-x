package org.geekhub.studentsregistry.web.mvc.controller;

import org.geekhub.studentsregistry.web.common.dto.StudentCountDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.geekhub.studentsregistry.web.mvc.controller.UserTestData.USER;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(MainController.class)
@Test(groups = {"integrationTest"})
public class MainControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @DataProvider(name = "shows_root_page_correct_urls")
    public Object[][] shows_root_page_correct_urls() {
        return new Object[][]{{"/"}, {"/main-menu"}};
    }

    @Test(dataProvider = "shows_root_page_correct_urls")
    public void shows_root_page_correct(String url) throws Exception {
        mockMvc.perform(get(url).with(USER))
            .andExpect(status().isOk())
            .andExpect(view().name("main-menu"))
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
            .andExpect(model().size(1))
            .andExpect(model().attribute("count", new StudentCountDTO()));
    }

    @Test
    public void shows_login_page_correct() throws Exception {
        mockMvc.perform(get("/login"))
            .andExpect(status().isOk())
            .andExpect(view().name("login"))
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
            .andExpect(model().size(0));
    }

}