package org.geekhub.studentsregistry.web.mvc.controller;

import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

public class UserTestData {
    public static final SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor USER =
        user("admin").password("admin").roles("ADMIN");
}
