package org.geekhub.studentsregistry.web.mvc.controller;

import org.geekhub.studentsregistry.grade.GpaGrade;
import org.geekhub.studentsregistry.student.Student;
import org.geekhub.studentsregistry.web.common.dto.CreateStudentDTO;
import org.geekhub.studentsregistry.web.common.mapper.StudentDTOMapper;
import org.geekhub.studentsregistry.web.common.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;

import static org.geekhub.studentsregistry.web.mvc.controller.UserTestData.USER;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(StudentController.class)
@Test(groups = {"integrationTest"})
public class StudentControllerTest extends AbstractTestNGSpringContextTests {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    @MockBean
    private StudentService studentService;

    @Autowired
    @MockBean
    private StudentDTOMapper studentDTOMapper;

    @Test
    public void works_correct_if_there_are_no_students() throws Exception {
        when(studentService.getStudentsFilteredByGradeType()).thenReturn(Collections.emptyMap());

        mockMvc.perform(MockMvcRequestBuilders.get("/students/all").with(USER))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
            .andExpect(view().name("students"))
            .andExpect(model().size(1))
            .andExpect(model().attribute("students", Collections.emptyMap()));
    }

    @Test
    public void shows_students_form_correct() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/students/new/manual").with(USER))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
            .andExpect(view().name("student-form"))
            .andExpect(model().size(1))
            .andExpect(model().attribute("student", new CreateStudentDTO()));
    }

    @Test
    public void generating_works_when_passed_positive_number() throws Exception {
        doNothing().when(studentService).generateStudents(5);

        mockMvc.perform(post("/students/new/auto").with(USER).with(csrf()).param("count", "5"))
            .andExpect(status().isFound())
            .andExpect(redirectedUrl("/students/all"));
    }

    @Test
    public void saves_student_when_passed_all_parameters() throws Exception {

        Student student = new Student("name", new GpaGrade(100), LocalDate.parse("2020-03-03"), LocalTime.parse("10:10"));
        doNothing().when(studentService)
            .createStudent(student);

        mockMvc.perform(
            post("/students/new")
                .with(USER).with(csrf())
                .flashAttr("student", new CreateStudentDTO("name", 100, "gpa", "2020-03-03", "10:10"))
        )
            .andExpect(status().isFound())
            .andExpect(redirectedUrl("/students/all"));
    }

    @Test
    public void deletes_students() throws Exception {
        doNothing().when(studentService).deleteAllStudents();
        mockMvc.perform(post("/students/delete/all").with(USER).with(csrf()))
            .andDo(print())
            .andExpect(status().isFound())
            .andExpect(redirectedUrl("/students/all"));
    }

}