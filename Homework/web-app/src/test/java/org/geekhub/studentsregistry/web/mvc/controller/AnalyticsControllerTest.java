package org.geekhub.studentsregistry.web.mvc.controller;

import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.web.common.dto.AnalyticsDTO;
import org.geekhub.studentsregistry.web.common.service.StudentAnalyticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import static org.geekhub.studentsregistry.web.mvc.controller.UserTestData.USER;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(AnalyticsController.class)
@Test(groups = {"integrationTest"})
public class AnalyticsControllerTest extends AbstractTestNGSpringContextTests {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    @MockBean
    private StudentAnalyticsService analyticsService;

    @Test
    public void shows_result_correct() throws Exception {
        AnalyticsDTO emptyResult = new AnalyticsDTO("N/A", "N/A", "N/A");
        Map<String, AnalyticsDTO> result = Arrays.stream(GradeType.values())
            .collect(Collectors.toMap(Enum::name, gradeType -> emptyResult));
        when(analyticsService.analyticsInEachCategory()).thenReturn(result);

        mockMvc.perform(MockMvcRequestBuilders.get("/students/analytics").with(USER))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
            .andExpect(view().name("analytics"))
            .andExpect(model().size(1))
            .andExpect(model().attribute("analytics", result));
    }

}