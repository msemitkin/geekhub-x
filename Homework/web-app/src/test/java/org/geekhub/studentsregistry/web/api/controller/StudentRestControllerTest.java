package org.geekhub.studentsregistry.web.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.geekhub.studentsregistry.grade.GpaGrade;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.grade.LetterGrade;
import org.geekhub.studentsregistry.grade.PercentageGrade;
import org.geekhub.studentsregistry.grade.UkrainianGrade;
import org.geekhub.studentsregistry.student.Student;
import org.geekhub.studentsregistry.web.common.dto.StudentDto;
import org.geekhub.studentsregistry.web.common.mapper.StudentDTOMapper;
import org.geekhub.studentsregistry.web.common.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(StudentRestController.class)
@Test(groups = {"integrationTest"})
public class StudentRestControllerTest extends AbstractTestNGSpringContextTests {

    private static final Map<GradeType, List<Student>> TEST_STUDENTS =
        Map.of(
            GradeType.GPA, List.of(new Student("name1", new GpaGrade(100), LocalDate.parse("2020-01-01"), LocalTime.parse("10:10"))),
            GradeType.LETTER, List.of(new Student("name2", new LetterGrade(100), LocalDate.parse("2020-01-01"), LocalTime.parse("10:10"))),
            GradeType.PERCENTAGE, List.of(new Student("name3", new PercentageGrade(100), LocalDate.parse("2020-01-01"), LocalTime.parse("10:10"))),
            GradeType.UKRAINIAN, List.of(new Student("name4", new UkrainianGrade(100), LocalDate.parse("2020-01-01"), LocalTime.parse("10:10")))
        );
    private static final Map<GradeType, List<StudentDto>> TEST_STUDENTS_DTOS =
        Map.of(
            GradeType.GPA, List.of(new StudentDto("name1", "4.0", "2020-01-01", "10:10")),
            GradeType.LETTER, List.of(new StudentDto("name2", "A", "2020-01-01", "10:10")),
            GradeType.PERCENTAGE, List.of(new StudentDto("name3", "90 - 100%", "2020-01-01", "10:10")),
            GradeType.UKRAINIAN, List.of(new StudentDto("name4", "12", "2020-01-01", "10:10"))
        );

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    @MockBean
    private StudentService studentService;

    @Autowired
    @MockBean
    private StudentDTOMapper studentDTOMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void generates_students_correct() throws Exception {
        int count = 4;

        doNothing().when(studentService).generateStudents(count);
        when(studentService.getStudentsFilteredByGradeType()).thenReturn(TEST_STUDENTS);
        mockMvc.perform(post("/api/students/generator").param("count", String.valueOf(count)))
            .andExpect(status().isCreated());
    }

    @Test
    public void saves_student_correct() throws Exception {
        mockMvc.perform(
            post("/api/students")
                .param("name", "name")
                .param("score", "100")
                .param("gradeType", "gpa")
                .param("date", "2020-03-03")
                .param("time", "10:10")
        )
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void returns_students_correct() throws Exception {
        when(studentService.getStudentsFilteredByGradeType()).thenReturn(TEST_STUDENTS);

        for (GradeType gradeType : GradeType.values()) {
            when(studentDTOMapper.toDTO(TEST_STUDENTS.get(gradeType).get(0))).thenReturn(TEST_STUDENTS_DTOS.get(gradeType).get(0));
        }

        ResultActions perform = mockMvc.perform(get("/api/students"));

        perform
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$").isMap());

        for (GradeType gradeType : GradeType.values()) {
            String path = "$." + gradeType.name();
            perform.andExpect(jsonPath(path).isArray());
        }
    }

    @Test
    public void deletes_student_correct() throws Exception {
        int id = 1;
        doNothing().when(studentService).deleteById(id);
        mockMvc.perform(delete("/api/students/{id}", id))
            .andExpect(status().isOk());

    }

    @Test
    public void deletes_all_students_correct() throws Exception {
        doNothing().when(studentService).deleteAllStudents();
        mockMvc.perform(delete("/api/students"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").doesNotExist());
    }
}