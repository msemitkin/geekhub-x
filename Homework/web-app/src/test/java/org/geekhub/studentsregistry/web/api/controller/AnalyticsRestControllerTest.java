package org.geekhub.studentsregistry.web.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.web.common.dto.AnalyticsDTO;
import org.geekhub.studentsregistry.web.common.service.StudentAnalyticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AnalyticsRestController.class)
@Test(groups = {"integrationTest"})
public class AnalyticsRestControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    @MockBean
    private StudentAnalyticsService analyticsService;

    @Test
    public void returns_students_analytics() throws Exception {
        Map<String, AnalyticsDTO> result = Arrays.stream(GradeType.values())
            .collect(Collectors.toMap(Enum::name, gradeType -> new AnalyticsDTO("N/A", "N/A", "N/A")));
        when(analyticsService.analyticsInEachCategory()).thenReturn(result);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/analytics"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$").isMap())
            .andExpect(content().string(objectMapper.writeValueAsString(result)));
    }

}