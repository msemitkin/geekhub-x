package org.geekhub.studentsregistry.grade;

public class GradeTestData {

    public static final GradeAdapter LETTER_GRADE = new LetterGrade(100);

    public static final GradeAdapter PERCENTAGE_GRADE = new PercentageGrade(100);

    public static final GradeAdapter GPA_GRADE = new GpaGrade(100);

    public static final GradeAdapter UKRAINIAN_GRADE = new UkrainianGrade(100);

}
