package org.geekhub.studentsregistry;

import java.time.LocalDate;
import java.time.LocalTime;

public class DateTimeData {
    public static final LocalDate DATE = LocalDate.parse("2019-05-20");
    public static final LocalTime TIME = LocalTime.parse("16:20");
}
