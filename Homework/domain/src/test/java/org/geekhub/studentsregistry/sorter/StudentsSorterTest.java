package org.geekhub.studentsregistry.sorter;

import org.geekhub.studentsregistry.grade.GpaGrade;
import org.geekhub.studentsregistry.grade.UkrainianGrade;
import org.geekhub.studentsregistry.student.Student;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.geekhub.studentsregistry.DateTimeData.DATE;
import static org.geekhub.studentsregistry.DateTimeData.TIME;
import static org.geekhub.studentsregistry.grade.GradeTestData.GPA_GRADE;
import static org.geekhub.studentsregistry.grade.GradeTestData.PERCENTAGE_GRADE;

public class StudentsSorterTest {
    private StudentsSorter studentsSorter;

    @BeforeMethod
    private void setUp() {
        studentsSorter = new StudentsSorter();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void students_cant_be_sorted_for_null_input() {
        studentsSorter.sortedStudents(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void students_cant_be_sorted_for_empty_input() {
        List<Student> students = new ArrayList<>();
        studentsSorter.sortedStudents(students);
    }

    @Test
    public void students_sorted_with_only_one_student_in_input() {
        Student student = new Student("a", PERCENTAGE_GRADE, DATE, TIME);
        List<Student> students = new ArrayList<>();
        students.add(student);

        List<Student> sortedStudents = studentsSorter.sortedStudents(students);

        Assert.assertEquals(students, sortedStudents);
    }

    @Test
    public void students_sorted_by_grades_with_unique_values() {
        Student student80 = new Student("a", new GpaGrade(80), DATE, TIME);
        Student student90 = new Student("b", new GpaGrade(90), DATE, TIME);
        Student student70 = new Student("c", new GpaGrade(70), DATE, TIME);

        List<Student> studentsWithUniqueScores = List.of(
            student80, student90, student70
        );

        List<Student> sortedStudents = studentsSorter.sortedStudents(studentsWithUniqueScores);

        List<Student> expected = List.of(
            student90, student80, student70
        );
        Assert.assertEquals(expected, sortedStudents);
    }


    @Test
    public void students_sorted_by_grades_with_same_values() {
        List<Student> studentsWithSameScores = List.of(
            new Student("a", GPA_GRADE, DATE, TIME),
            new Student("c", GPA_GRADE, DATE, TIME),
            new Student("b", GPA_GRADE, DATE, TIME)
        );

        List<Student> sortedStudents = studentsSorter.sortedStudents(studentsWithSameScores);

        List<Student> expected = List.of(
            new Student("a", GPA_GRADE, DATE, TIME),
            new Student("b", GPA_GRADE, DATE, TIME),
            new Student("c", GPA_GRADE, DATE, TIME)
        );

        Assert.assertEquals(expected, sortedStudents);
    }

    @Test
    public void students_sorted_with_grades_in_one_score_range() {
        Student studentWith90 = new Student("a", new GpaGrade(90), DATE, TIME);
        Student studentWith98 = new Student("b", new GpaGrade(98), DATE, TIME);
        Student studentWith100 = new Student("c", new GpaGrade(100), DATE, TIME);

        List<Student> students = List.of(
            studentWith90, studentWith98, studentWith100
        );

        List<Student> sortedStudents = studentsSorter.sortedStudents(students);

        List<Student> expected = List.of(
            studentWith90, studentWith98, studentWith100
        );

        Assert.assertEquals(expected, sortedStudents);
    }

    @Test
    public void students_with_ukrainian_grades_are_sorted_correctly() {
        Student studentWith1 = new Student("a", new UkrainianGrade(0), DATE, TIME);
        Student studentWith2 = new Student("c", new UkrainianGrade(5), DATE, TIME);
        Student studentWith10 = new Student("d", new UkrainianGrade(89), DATE, TIME);
        Student studentWith11 = new Student("e", new UkrainianGrade(99), DATE, TIME);
        Student studentWith12 = new Student("f", new UkrainianGrade(100), DATE, TIME);
        List<Student> sortedStudents = studentsSorter.sortedStudents(
            List.of(studentWith1, studentWith2, studentWith10, studentWith11, studentWith12));
        List<Student> expected =
            List.of(studentWith12, studentWith11, studentWith10, studentWith2, studentWith1);
        Assert.assertEquals(sortedStudents, expected);
    }

}
