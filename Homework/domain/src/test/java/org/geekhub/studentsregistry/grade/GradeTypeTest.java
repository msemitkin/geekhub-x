package org.geekhub.studentsregistry.grade;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class GradeTypeTest {

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void throws_IllegalArgumentException_when_null_input() {
        GradeType.from(null);
    }

    @Test(expectedExceptions = ParameterNotSpecifiedException.class)
    public void throws_ParameterNotSpecifiedException_when_input_string_is_empty() {
        GradeType.from("");
    }

    @Test(expectedExceptions = GradeNotSupportedException.class)
    public void throws_GradeNotSupportedException_when_grade_is_not_supported() {
        GradeType.from("gap");
    }

    @DataProvider(name = "returns_grade_of_correct_type_parameters")
    public Object[][] returns_grade_of_correct_type_parameters() {
        return new Object[][]{
            {"LETTER", GradeType.LETTER},
            {"letter", GradeType.LETTER},
            {"PERCENTAGE", GradeType.PERCENTAGE},
            {"percentage", GradeType.PERCENTAGE},
            {"GPA", GradeType.GPA},
            {"gpa", GradeType.GPA},
        };
    }

    @Test(dataProvider = "returns_grade_of_correct_type_parameters")
    public void returns_grade_of_correct_type(String inputGradeType, GradeType expectedGradeType) {
        Assert.assertEquals(GradeType.from(inputGradeType), expectedGradeType);
    }

    @DataProvider(name = "test_returns_correct_GradeType_parameters")
    public Object[][] test_returns_correct_GradeType_parameters() {
        return new Object[][]{
            {GradeTestData.GPA_GRADE, GradeType.GPA},
            {GradeTestData.LETTER_GRADE, GradeType.LETTER},
            {GradeTestData.PERCENTAGE_GRADE, GradeType.PERCENTAGE},
            {GradeTestData.UKRAINIAN_GRADE, GradeType.UKRAINIAN}
        };
    }

    @Test(dataProvider = "test_returns_correct_GradeType_parameters")
    public void test_returns_correct_GradeType(Grade grade, GradeType gradeType) {
        Assert.assertEquals(GradeType.resolveGradeType(grade), gradeType);
    }
}
