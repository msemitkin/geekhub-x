package org.geekhub.studentsregistry.grade;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class UkrainianGradeTest {
    @DataProvider(name = "test_asPrintVersion_parameters")
    private Object[][] test_asPrintVersion_parameters() {
        return new Object[][]{
            {0, "1"},
            {5, "2"},
            {10, "3"},
            {20, "4"},
            {30, "5"},
            {40, "6"},
            {50, "7"},
            {60, "8"},
            {70, "9"},
            {80, "10"},
            {90, "11"},
            {100, "12"},
        };
    }

    @Test(dataProvider = "test_asPrintVersion_parameters")
    public void test_asPrintVersion(int score, String printVersion) {
        Assert.assertEquals(new UkrainianGrade(score).asPrintVersion(), printVersion);
    }

    @DataProvider(name = "test_asPrintVersionThrows_InvalidScoreException_when_score_is_not_in_allowed_range_parameters")
    private Object[][] test_asPrintVersionThrows_InvalidScoreException_when_score_is_not_in_allowed_range_parameters() {
        return new Object[][]{
            {101}, {-1}
        };
    }

    @Test(expectedExceptions = InvalidScoreException.class,
        dataProvider = "test_asPrintVersionThrows_InvalidScoreException_when_score_is_not_in_allowed_range_parameters")
    public void test_asPrintVersionThrows_InvalidScoreException_when_score_is_not_in_allowed_range(int score) {
        new UkrainianGrade(score).asPrintVersion();
    }
}
