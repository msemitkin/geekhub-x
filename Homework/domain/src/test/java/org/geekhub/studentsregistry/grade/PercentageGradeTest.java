package org.geekhub.studentsregistry.grade;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PercentageGradeTest {

    @DataProvider(name = "test_asPrintVersion_parameters")
    private Object[][] test_asPrintVersion_parameters() {
        return new Object[][]{
            {90, "90 - 100%"},
            {80, "80 - 89%"},
            {70, "70 - 79%"},
            {60, "60 - 69%"},
            {50, "< 60%"}
        };
    }

    @DataProvider(
        name = "test_as_print_version_throws_InvalidScoreException_with_invalid_scores_parameters"
    )
    private Object[][]
    test_as_print_version_throws_InvalidScoreException_when_score_is_not_in_allowed_range_parameters() {
        return new Object[][]{
            {101},
            {-1}
        };
    }

    @Test(dataProvider = "test_asPrintVersion_parameters")
    public void test_asPrintVersion(int gradeValue, String expectedOutput) {
        Grade grade = new PercentageGrade(gradeValue);
        String actualOutput = grade.asPrintVersion();
        assertEquals(actualOutput, expectedOutput);
    }

    @Test(expectedExceptions = InvalidScoreException.class,
        dataProvider = "test_as_print_version_throws_InvalidScoreException_with_invalid_scores_parameters")
    public void print_version_of_grade_throws_InvalidScoreException_when_score_is_more_than_100(int gradeValue) {
        Grade grade = new PercentageGrade(gradeValue);
        grade.asPrintVersion();
    }

}