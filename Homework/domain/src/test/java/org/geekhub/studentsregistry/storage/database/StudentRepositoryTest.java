package org.geekhub.studentsregistry.storage.database;

import org.assertj.core.api.Assertions;
import org.geekhub.studentsregistry.grade.GradeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

import static org.geekhub.studentsregistry.DateTimeData.DATE;
import static org.geekhub.studentsregistry.DateTimeData.TIME;

@DataJpaTest
@Test(groups = {"integrationTest"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class StudentRepositoryTest extends AbstractTestNGSpringContextTests {

    private static final StudentEntity STUDENT =
        new StudentEntity(null, "student1", 100, GradeType.GPA, DATE, TIME);

    @Autowired
    private StudentRepository studentRepository;


    @Test
    public void repository_is_empty() {
        Assertions.assertThat(studentRepository.count()).isZero();
    }

    @DataProvider(name = "returns_true_when_user_exists_with_given_name_ignore_case_parameters")
    private Object[][] returns_true_when_user_exists_with_given_name_ignore_case_parameters() {
        return new Object[][]{
            {"name"},
            {"NAME"},
            {"Name"}
        };
    }

    @Test(dataProvider = "returns_true_when_user_exists_with_given_name_ignore_case_parameters")
    public void returns_true_when_user_exists_with_given_name_ignore_case(String name) {
        StudentEntity student = new StudentEntity(null, name, 100, GradeType.GPA, DATE, TIME);
        studentRepository.save(student);
        Assert.assertTrue(studentRepository.existsStudentEntityByNameIgnoreCase("name"));
    }

    @Test
    public void returns_empty_result_when_there_is_no_entity_with_given_id() {
        Assert.assertTrue(studentRepository.findById(1).isEmpty());
    }

    @Test
    public void returns_correct_entity_with_given_id_when_present() {
        StudentEntity saved = studentRepository.save(STUDENT);
        Assert.assertEquals(studentRepository.findById(1), Optional.of(saved));

    }

    @Test
    public void returns_correct_entity_when_there_are_multiple_entities_saved() {
        studentRepository.save(new StudentEntity(null, "student1", 100, GradeType.GPA, DATE, TIME));
        StudentEntity saved = studentRepository.save(new StudentEntity(null, "student2", 100, GradeType.GPA, DATE, TIME));
        studentRepository.save(new StudentEntity(null, "student3", 100, GradeType.GPA, DATE, TIME));

        Assert.assertEquals(studentRepository.findById(2), Optional.of(saved));
    }


    @Test
    public void returns_false_when_there_is_no_user_with_given_name_ignorecase() {
        Assert.assertFalse(studentRepository.existsStudentEntityByNameIgnoreCase("name"));
    }

    @Test
    public void throws_EmptyResultDataAccessException_when_trying_to_delete_student_with_not_existing_id() {
        Assertions.assertThatCode(
            () -> studentRepository.deleteById(1)
        ).isInstanceOf(EmptyResultDataAccessException.class);
    }

    @Test
    public void deletes_student_by_id_correct() {
        studentRepository.save(STUDENT);
        Assertions.assertThat(studentRepository.count()).isEqualTo(1);
        studentRepository.deleteById(1);
        Assertions.assertThat(studentRepository.count()).isZero();
    }

    @Test
    public void does_not_throw_any_exception_when_trying_to_delete_not_existing_entity() {
        StudentEntity student = new StudentEntity(1, null, null, null, null, null);
        Assertions.assertThatCode(
            () -> studentRepository.delete(student)
        ).doesNotThrowAnyException();

    }

    @Test
    public void deletes_student_correct() {
        StudentEntity studentEntity = studentRepository.save(STUDENT);
        Assertions.assertThat(studentRepository.count()).isEqualTo(1);
        studentRepository.delete(studentEntity);
        Assertions.assertThat(studentRepository.count()).isZero();
    }

    @Test
    public void finds_multiple_students() {
        StudentEntity firstStudent = studentRepository.save(
            new StudentEntity(null, "name1", 100, GradeType.GPA, DATE, TIME));
        StudentEntity secondStudent = studentRepository.save(
            new StudentEntity(null, "name1", 100, GradeType.GPA, DATE, TIME));

        Assertions.assertThat(studentRepository.findAll())
            .isEqualTo(List.of(firstStudent, secondStudent));
    }
}