package org.geekhub.studentsregistry.filterer;

import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.student.Student;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.geekhub.studentsregistry.DateTimeData.DATE;
import static org.geekhub.studentsregistry.DateTimeData.TIME;
import static org.geekhub.studentsregistry.grade.GradeTestData.GPA_GRADE;
import static org.geekhub.studentsregistry.grade.GradeTestData.LETTER_GRADE;
import static org.geekhub.studentsregistry.grade.GradeTestData.PERCENTAGE_GRADE;
import static org.geekhub.studentsregistry.grade.GradeTestData.UKRAINIAN_GRADE;
import static org.testng.Assert.assertEquals;

public class StudentsFiltererTest {

    private StudentsFilterer studentsFilterer;

    @BeforeMethod
    private void setUp() {
        studentsFilterer = new StudentsFilterer();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void students_cant_be_filtered_for_empty_list() {
        List<Student> students = Collections.emptyList();
        studentsFilterer.getStudentsFilteredByGradeType(students);
    }

    @Test
    public void filtered_students_are_only_with_one_grade_type() {
        List<Student> students = List.of(
            new Student("a", GPA_GRADE, DATE, TIME),
            new Student("b", GPA_GRADE, DATE, TIME),
            new Student("c", GPA_GRADE, DATE, TIME)
        );
        Map<GradeType, List<Student>> filteredStudents =
            studentsFilterer.getStudentsFilteredByGradeType(students);

        Map<GradeType, List<Student>> expected = Map.of(
            GradeType.GPA, students
        );
        assertEquals(filteredStudents, expected);
    }


    @Test
    public void students_are_filtered_when_input_contain_different_grade_types() {
        List<Student> students = List.of(
            new Student("a", GPA_GRADE, DATE, TIME),
            new Student("b", PERCENTAGE_GRADE, DATE, TIME),
            new Student("c", LETTER_GRADE, DATE, TIME),
            new Student("e", GPA_GRADE, DATE, TIME),
            new Student("f", UKRAINIAN_GRADE, DATE, TIME),
            new Student("z", UKRAINIAN_GRADE, DATE, TIME)
        );

        Map<GradeType, List<Student>> filteredStudents =
            studentsFilterer.getStudentsFilteredByGradeType(students);

        Map<GradeType, List<Student>> expectedStudents = Map.of(
            GradeType.LETTER, List.of(new Student("c", LETTER_GRADE, DATE, TIME)),
            GradeType.PERCENTAGE, List.of(new Student("b", PERCENTAGE_GRADE, DATE, TIME)),
            GradeType.GPA, List.of(new Student("a", GPA_GRADE, DATE, TIME),
                new Student("e", GPA_GRADE, DATE, TIME)),
            GradeType.UKRAINIAN, List.of(new Student("f", UKRAINIAN_GRADE, DATE, TIME),
                new Student("z", UKRAINIAN_GRADE, DATE, TIME))
        );

        assertEquals(filteredStudents, expectedStudents);
    }
}
