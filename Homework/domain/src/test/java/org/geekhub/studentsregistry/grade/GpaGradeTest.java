package org.geekhub.studentsregistry.grade;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class GpaGradeTest {

    @DataProvider(name = "test_asPrintVersion_parameters")
    private Object[][] test_asPrintVersion_parameters() {
        return new Object[][]{
            {90, "4.0"},
            {80, "3.0"},
            {70, "2.0"},
            {60, "1.0"},
            {50, "0.0"}
        };
    }

    @DataProvider(
        name = "test_asPrintVersion_throws_InvalidScoreException_with_invalid_scores_parameters"
    )
    private Object[][]
    test_asPrintVersion_throws_InvalidScoreException_with_invalid_scores_parameters() {
        return new Object[][]{
            {101},
            {-1}
        };
    }

    @Test(dataProvider = "test_asPrintVersion_parameters")
    public void test_asPrintVersion(int gradeValue, String expectedOutput) {
        Grade grade = new GpaGrade(gradeValue);
        String actualOutput = grade.asPrintVersion();
        assertEquals(actualOutput, expectedOutput);
    }

    @Test(expectedExceptions = InvalidScoreException.class,
        dataProvider = "test_asPrintVersion_throws_InvalidScoreException_with_invalid_scores_parameters")
    public void test_asPrintVersion_throws_InvalidScoreException_with_invalid_scores(int gradeValue) {
        Grade grade = new GpaGrade(gradeValue);
        grade.asPrintVersion();
    }

}
