package org.geekhub.studentsregistry.reader;

import org.geekhub.studentsregistry.grade.ParameterNotSpecifiedException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class InputModeTest {

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void throws_IllegalArgumentException_for_null_parameter() {
        InputMode.from(null);
    }

    @Test(expectedExceptions = ParameterNotSpecifiedException.class)
    public void throws_ParameterNotSpecifiedException_for_empty_parameter() {
        InputMode.from("");
    }

    @Test(expectedExceptions = InputModeNotSupportedException.class)
    public void throws_InputModeNotSupportedException_when_InputMode_is_not_supported() {
        InputMode.from("random");
    }

    @DataProvider(name = "test_returns_correct_InputMode_parameters")
    public Object[][] test_returns_correct_InputMode_parameters() {
        return new Object[][]{
            {"AUTO", InputMode.AUTO},
            {"auto", InputMode.AUTO},
            {"MANUAL", InputMode.MANUAL},
            {"manual", InputMode.MANUAL},
        };
    }

    @Test(dataProvider = "test_returns_correct_InputMode_parameters")
    public void test_returns_correct_InputMode(String receivedMode, InputMode expectedMode) {
        Assert.assertEquals(expectedMode, InputMode.from(receivedMode));
    }
}