package org.geekhub.studentsregistry;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;

@TestConfiguration
@ComponentScan("org.geekhub.studentsregistry")
public class CommonConfig {
}
