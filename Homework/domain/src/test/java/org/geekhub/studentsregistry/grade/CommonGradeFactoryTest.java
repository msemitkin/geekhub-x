package org.geekhub.studentsregistry.grade;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class CommonGradeFactoryTest {
    CommonGradeFactory commonGradeFactory;

    @BeforeMethod
    private void setUp() {
        commonGradeFactory = new CommonGradeFactory();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void cant_create_grade_instance_for_null_grade_type() {
        commonGradeFactory.createGrade(null, 100);
    }

    @Test
    public void letter_grade_is_created_for_grade_type_LETTER() {
        Grade grade = commonGradeFactory.createGrade(GradeType.LETTER, 100);

        assertTrue(grade instanceof LetterGrade);
    }

    @Test
    public void letter_grade_is_created_for_grade_type_PERCENTAGE() {
        Grade grade = commonGradeFactory.createGrade(GradeType.PERCENTAGE, 100);

        assertTrue(grade instanceof PercentageGrade);
    }

    @Test
    public void letter_grade_is_created_for_grade_type_GPA() {
        Grade grade = commonGradeFactory.createGrade(GradeType.GPA, 100);

        assertTrue(grade instanceof GpaGrade);
    }

    @Test
    public void letter_grade_is_created_for_grade_type_UKRAINIAN() {
        Grade grade = commonGradeFactory.createGrade(GradeType.UKRAINIAN, 100);

        assertTrue(grade instanceof UkrainianGrade);
    }
}