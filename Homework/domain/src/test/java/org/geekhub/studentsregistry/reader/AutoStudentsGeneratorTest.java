package org.geekhub.studentsregistry.reader;

import org.assertj.core.api.Assertions;
import org.geekhub.studentsregistry.student.Student;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Random;

public class AutoStudentsGeneratorTest {

    AutoStudentsGenerator autoStudentsGenerator;

    @BeforeMethod
    private void setUp() {
        autoStudentsGenerator = new AutoStudentsGenerator(new Random());
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void readStudents_throws_IllegalArgumentException_when_totalStudentCount_is_not_a_positive_number() {
        autoStudentsGenerator.readStudents(0);
    }

    @Test
    public void generated_list_is_not_null() {
        Assertions.assertThat(autoStudentsGenerator.readStudents(10)).isNotNull();
    }

    @Test
    public void generated_students_are_not_null() {
        autoStudentsGenerator.readStudents(10).forEach(Assert::assertNotNull);
    }

    @Test
    public void generated_students_are_with_all_required_fields_not_null() {
        autoStudentsGenerator.readStudents(10).forEach(
            student -> Assertions.assertThat(student).hasNoNullFieldsOrProperties());
    }

    @Test
    public void generated_list_has_required_number_of_students() {
        int totalStudentsCount = 10;
        List<Student> students = autoStudentsGenerator.readStudents(totalStudentsCount);
        Assert.assertEquals(students.size(), totalStudentsCount);
    }
}