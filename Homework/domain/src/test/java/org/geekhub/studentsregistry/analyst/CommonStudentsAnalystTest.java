package org.geekhub.studentsregistry.analyst;

import org.geekhub.studentsregistry.CommonConfig;
import org.geekhub.studentsregistry.DateTimeData;
import org.geekhub.studentsregistry.grade.GpaGrade;
import org.geekhub.studentsregistry.grade.GradeAdapter;
import org.geekhub.studentsregistry.grade.GradeTestData;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.grade.LetterGrade;
import org.geekhub.studentsregistry.grade.UkrainianGrade;
import org.geekhub.studentsregistry.mapper.StudentMapper;
import org.geekhub.studentsregistry.storage.database.StudentEntity;
import org.geekhub.studentsregistry.storage.database.StudentRepository;
import org.geekhub.studentsregistry.student.Student;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@DataJpaTest
@ContextConfiguration(classes = CommonConfig.class)
@Test(groups = {"integrationTest"})
public class CommonStudentsAnalystTest extends AbstractTestNGSpringContextTests {

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    CommonStudentsAnalyst studentsAnalyst;

    @Autowired
    StudentMapper studentMapper;

    @BeforeMethod
    public void setUp() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate.getJdbcTemplate(), "student");
    }

    @Test
    public void dependencies_are_not_null() {
        Assertions.assertAll(
            () -> Assert.assertNotNull(jdbcTemplate),
            () -> Assert.assertNotNull(studentsAnalyst),
            () -> Assert.assertNotNull(studentRepository)
        );
    }

    @Test
    public void values_are_not_present_when_database_is_empty() {
        GradeType gradeType = GradeType.GPA;

        Assertions.assertAll(
            () -> Assert.assertTrue(studentsAnalyst.maxScore(gradeType).isEmpty()),
            () -> Assert.assertTrue(studentsAnalyst.minScore(gradeType).isEmpty()),
            () -> Assert.assertTrue(studentsAnalyst.medianScore(gradeType).isEmpty())
        );
    }

    @Test
    public void values_are_empty_when_there_are_no_students_with_grades_of_requested_type() {
        Student student1 = new Student("student1", GradeTestData.GPA_GRADE, DateTimeData.DATE, DateTimeData.TIME);
        Student student2 = new Student("student2", GradeTestData.LETTER_GRADE, DateTimeData.DATE, DateTimeData.TIME);
        Student student3 = new Student("student3", GradeTestData.PERCENTAGE_GRADE, DateTimeData.DATE, DateTimeData.TIME);

        List<StudentEntity> entities = List.of(student1, student2, student3).stream()
            .map(studentMapper::toEntity)
            .collect(Collectors.toList());
        studentRepository.saveAll(entities);

        Assertions.assertAll(
            () -> Assert.assertTrue(studentsAnalyst.maxScore(GradeType.UKRAINIAN).isEmpty()),
            () -> Assert.assertTrue(studentsAnalyst.minScore(GradeType.UKRAINIAN).isEmpty()),
            () -> Assert.assertTrue(studentsAnalyst.medianScore(GradeType.UKRAINIAN).isEmpty())
        );
    }

    @DataProvider(name = "values_are_correct_provider")
    private Object[][] values_are_correct_provider() {
        return new Object[][]{
            {
                List.of(new UkrainianGrade(80)),
                GradeType.UKRAINIAN,
                new Analytics(Optional.of(80), Optional.of(80), Optional.of(80))
            },
            {
                List.of(new UkrainianGrade(50), new UkrainianGrade(60)),
                GradeType.UKRAINIAN,
                new Analytics(Optional.of(50), Optional.of(60), Optional.of(50))
            },
            {
                List.of(new GpaGrade(100), new GpaGrade(90), new GpaGrade(80)),
                GradeType.GPA,
                new Analytics(Optional.of(80), Optional.of(100), Optional.of(90))
            },
            {
                List.of(new LetterGrade(100), new LetterGrade(90),
                    new LetterGrade(80), new LetterGrade(70)),
                GradeType.LETTER,
                new Analytics(Optional.of(70), Optional.of(100), Optional.of(80))
            },
            {
                List.of(new LetterGrade(80), new LetterGrade(70),
                    new LetterGrade(70), new LetterGrade(60)),
                GradeType.LETTER,
                new Analytics(Optional.of(60), Optional.of(80), Optional.of(70))
            }
        };
    }

    @Test(dataProvider = "values_are_correct_provider")
    public void values_are_correct(List<GradeAdapter> grades, GradeType category, Analytics expected) {
        grades.forEach(grade -> studentRepository.save(studentMapper.toEntity(new Student("name", grade, DateTimeData.DATE, DateTimeData.TIME))));

        Analytics actual = new Analytics(
            studentsAnalyst.minScore(category),
            studentsAnalyst.maxScore(category),
            studentsAnalyst.medianScore(category)
        );

        Assert.assertEquals(actual, expected);
    }

}