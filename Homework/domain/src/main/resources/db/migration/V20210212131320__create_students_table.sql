CREATE TABLE student
(
    id         serial primary key,
    name       varchar(256) not null,
    grade_type varchar(256) not null,
    score      integer      not null,
    date       date         not null,
    time       time         not null
);