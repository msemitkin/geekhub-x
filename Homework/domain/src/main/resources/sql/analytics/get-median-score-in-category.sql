WITH selected_students AS
         (SELECT score,
                 row_number() OVER (
          ORDER BY score desc) AS rnum
FROM student
WHERE grade_type = :grade_type)
SELECT score
FROM selected_students
WHERE rnum = (SELECT floor(count(*) / 2) + 1 FROM selected_students);