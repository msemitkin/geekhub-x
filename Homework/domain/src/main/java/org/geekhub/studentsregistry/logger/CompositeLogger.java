package org.geekhub.studentsregistry.logger;

import java.util.List;

public class CompositeLogger implements Logger {
    private static List<Logger> loggers = List.of(new ConsoleLogger(), new FileLogger());

    @Override
    public void info(String message) {
        loggers.forEach(logger -> logger.info(message));
    }

    @Override
    public void error(String message) {
        loggers.forEach(logger -> logger.error(message));
    }
}
