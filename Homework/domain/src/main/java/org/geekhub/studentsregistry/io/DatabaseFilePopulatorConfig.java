package org.geekhub.studentsregistry.io;

import org.geekhub.studentsregistry.grade.CommonGradeFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;

@Configuration
public class DatabaseFilePopulatorConfig {

    @Bean
    StudentsFileReader studentsFileReader(CommonGradeFactory gradeFactory) {
        Path filePath = Path.of(System.getProperty("user.home")).resolve("students.csv");
        return new CSVStudentsFileReader(filePath, gradeFactory);
    }

}
