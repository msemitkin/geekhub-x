package org.geekhub.studentsregistry.printer;

import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.student.Student;

import java.util.List;

public interface StudentsPrinter {
    void printStudentsTable(List<Student> students);

    void printStudentsAnalyticsInCategory(GradeType gradeType);
}
