package org.geekhub.studentsregistry.cache;

import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Cache<K, V> {

    private Map<K, V> cacheMap = new ConcurrentHashMap<>();
    private final Integer invalidationTimeout;
    private final ScheduledExecutorService executorService;
    private final TimeUnit timeUnit;


    public Cache(
        ScheduledExecutorService executorService,
        Integer invalidationTimeoutMillis,
        TimeUnit timeUnit
    ) {
        this.invalidationTimeout = invalidationTimeoutMillis;
        this.executorService = executorService;
        this.timeUnit = timeUnit;
    }

    @PostConstruct
    public void setUp() {
        executorService.scheduleAtFixedRate(
            cacheMap::clear,
            invalidationTimeout,
            invalidationTimeout,
            timeUnit
        );
    }

    public boolean isNotEmpty() {
        return !cacheMap.isEmpty();
    }

    public Map<K, V> getAll() {
        return cacheMap;
    }

    public void update(Map<K, V> newMap) {
        Assert.notNull(newMap, "Null map received");
        cacheMap = new ConcurrentHashMap<>(newMap);
    }

    public void invalidate() {
        cacheMap.clear();
    }
}
