package org.geekhub.studentsregistry.reader;

import org.geekhub.studentsregistry.student.Student;
import java.util.List;

public interface StudentsReader {
    List<Student> readStudents(int count);
}
