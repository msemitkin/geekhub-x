package org.geekhub.studentsregistry.sorter;

import org.geekhub.studentsregistry.student.Student;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class StudentsSorter {
    public List<Student> sortedStudents(List<Student> students) {
        if (students == null || students.isEmpty()) {
            throw new IllegalArgumentException();
        }
        List<Student> sortedStudents = new ArrayList<>(students);
        Collections.sort(sortedStudents);
        return sortedStudents;
    }
}