package org.geekhub.studentsregistry.filterer;

import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.student.Student;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class StudentsFilterer {

    public Map<GradeType, List<Student>> getStudentsFilteredByGradeType(List<Student> students) {
        if (students == null || students.isEmpty()) {
            throw new IllegalArgumentException();
        }

        Map<GradeType, List<Student>> filteredStudents = new EnumMap<>(GradeType.class);

        for (Student student : students) {
            GradeType key = GradeType.resolveGradeType(student.grade());
            if (Objects.isNull(filteredStudents.get(key))) {
                filteredStudents.put(key, new ArrayList<>());
            }
            filteredStudents.get(key).add(student);
        }
        return filteredStudents;
    }
}
