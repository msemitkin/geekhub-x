package org.geekhub.studentsregistry.reader;

import java.util.Objects;
import org.geekhub.studentsregistry.grade.ParameterNotSpecifiedException;

public enum InputMode {
    AUTO, MANUAL;

    public static InputMode from(String requiredInputMode) {
        if (Objects.isNull(requiredInputMode)) {
            throw new IllegalArgumentException();
        }
        if (requiredInputMode.isBlank()) {
            throw new ParameterNotSpecifiedException();
        }
        for (InputMode inputMode : InputMode.values()) {
            if (inputMode.name().equalsIgnoreCase(requiredInputMode)) {
                return inputMode;
            }
        }
        throw new InputModeNotSupportedException();
    }
}