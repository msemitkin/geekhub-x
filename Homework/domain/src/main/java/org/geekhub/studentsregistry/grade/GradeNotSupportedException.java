package org.geekhub.studentsregistry.grade;

import org.geekhub.studentsregistry.InvalidInputException;

public class GradeNotSupportedException extends InvalidInputException {
    public GradeNotSupportedException() {
    }
}
