package org.geekhub.studentsregistry.grade;

import java.io.Serializable;
import java.util.Objects;

public abstract class GradeAdapter implements Grade, Comparable<GradeAdapter>, Serializable {
    int value;

    public int getGrade() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GradeAdapter that = (GradeAdapter) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "GradeAdapter{"
            + "value=" + value
            + '}';
    }
}
