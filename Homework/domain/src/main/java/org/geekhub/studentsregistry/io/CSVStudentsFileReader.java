package org.geekhub.studentsregistry.io;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.geekhub.studentsregistry.grade.CommonGradeFactory;
import org.geekhub.studentsregistry.grade.GradeAdapter;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.logger.Logger;
import org.geekhub.studentsregistry.logger.Logging;
import org.geekhub.studentsregistry.student.Student;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class CSVStudentsFileReader implements StudentsFileReader {
    private static final Logger logger = Logging.getLogger();
    private final Path filePath;
    private final CommonGradeFactory gradeFactory;

    public CSVStudentsFileReader(Path filePath, CommonGradeFactory gradeFactory) {
        this.filePath = filePath;
        this.gradeFactory = gradeFactory;
    }

    @Override
    public List<Student> getStudents() {
        try (
            FileReader fileReader = new FileReader(filePath.toString());
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            CSVReader csvReader = new CSVReader(bufferedReader)
        ) {
            logger.info("Students data file found");
            List<Student> students = parseStudents(csvReader);
            if (students.isEmpty()) {
                logger.info("Students file is empty");
            } else {
                logger.info("Students have been successfully parsed");
                return students;
            }
        } catch (FileNotFoundException e) {
            logger.info("File with students data not found");
        } catch (IOException e) {
            logger.error(String.format(
                "Failed to read data from file: %s%n%s", filePath, ExceptionUtils.getStackTrace(e)));
            throw new CSVFileException();
        }
        return Collections.emptyList();
    }

    private List<Student> parseStudents(CSVReader csvReader) {
        CsvToBeanBuilder<CSVStudent> csvToBeanBuilder = new CsvToBeanBuilder<>(csvReader);
        CsvToBean<CSVStudent> csvToBean = csvToBeanBuilder.withType(CSVStudent.class).build();
        return csvToBean.stream()
            .map(entry -> {
                    GradeAdapter grade = gradeFactory.createGrade(
                        GradeType.from(entry.getGradeType()),
                        entry.getScore());
                    return new Student(
                        entry.getName(),
                        grade,
                        LocalDate.parse(entry.getDate()),
                        LocalTime.parse(entry.getTime())
                    );
                }
            ).collect(Collectors.toList());
    }

}
