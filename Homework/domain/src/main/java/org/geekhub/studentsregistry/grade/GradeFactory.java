package org.geekhub.studentsregistry.grade;

public interface GradeFactory {
    Grade createGrade(GradeType gradeType, int value);
}
