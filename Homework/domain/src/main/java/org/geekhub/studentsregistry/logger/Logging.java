package org.geekhub.studentsregistry.logger;

public class Logging {
    private static CompositeLogger logger;

    private Logging() {
    }

    public static CompositeLogger getLogger() {
        if (logger == null) {
            logger = new CompositeLogger();
        }
        return logger;
    }
}
