package org.geekhub.studentsregistry.storage.database;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.geekhub.studentsregistry.logger.Logger;
import org.geekhub.studentsregistry.logger.Logging;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DatabaseQueries {

    private static final Logger logger = Logging.getLogger();

    private final Map<String, String> queries;
    private final String pattern;

    public DatabaseQueries(@Value("classpath*:sql/**/*.sql") String pattern) {
        this.queries = new HashMap<>();
        this.pattern = pattern;
    }

    @PostConstruct
    private void loadQueries() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(classLoader);
        try {
            Resource[] resources = resolver.getResources(pattern);
            for (Resource resource : resources) {
                try (InputStream inputStream = resource.getInputStream();
                     InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                     BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
                ) {
                    String query = bufferedReader.lines().collect(Collectors.joining("\n"));
                    queries.put(resource.getFilename(), query);
                }
            }
        } catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e));
            throw new InitializationException();
        }
    }

    public String getQuery(String fileName) {
        if (queries.containsKey(fileName)) {
            return queries.get(fileName);
        } else {
            throw new IllegalArgumentException();
        }
    }
}
