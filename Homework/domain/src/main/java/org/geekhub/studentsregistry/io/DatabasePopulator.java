package org.geekhub.studentsregistry.io;

import org.geekhub.studentsregistry.mapper.StudentMapper;
import org.geekhub.studentsregistry.storage.StudentsStorageManager;
import org.geekhub.studentsregistry.storage.database.StudentEntity;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class DatabasePopulator {
    private static final int INITIAL_DELAY = 5;
    private static final int PERIOD = 5;
    private static final TimeUnit TIME_UNIT = TimeUnit.MINUTES;

    private final StudentsFileReader studentsFileReader;
    private final StudentsStorageManager studentsStorageManager;
    private final StudentMapper studentMapper;

    public DatabasePopulator(
        StudentsFileReader studentsFileReader,
        StudentsStorageManager studentsStorageManager,
        StudentMapper studentMapper) {
        this.studentsFileReader = studentsFileReader;
        this.studentsStorageManager = studentsStorageManager;
        this.studentMapper = studentMapper;
    }

    @PostConstruct
    public void init() {
        ScheduledExecutorService executorService =
            Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(this::update, INITIAL_DELAY, PERIOD, TIME_UNIT);
    }


    public void update() {
        studentsFileReader.getStudents().forEach(
            student -> {
                if (!studentsStorageManager.existsWithNameIgnoreCase(student.name())) {
                    StudentEntity studentEntity = studentMapper.toEntity(student);
                    studentsStorageManager.save(studentEntity);
                }
            }
        );
    }
}
