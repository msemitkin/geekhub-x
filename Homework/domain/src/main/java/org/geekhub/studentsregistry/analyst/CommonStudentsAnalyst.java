package org.geekhub.studentsregistry.analyst;

import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.storage.database.DatabaseQueries;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommonStudentsAnalyst implements StudentsAnalyst {
    private static final String GRADE_TYPE_PARAMETER = "grade_type";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DatabaseQueries queries;

    public CommonStudentsAnalyst(
        NamedParameterJdbcTemplate jdbcTemplate,
        DatabaseQueries queries
    ) {
        this.jdbcTemplate = jdbcTemplate;
        this.queries = queries;
    }

    @Override
    public Optional<Integer> maxScore(GradeType category) {
        return Optional.ofNullable(jdbcTemplate
            .queryForObject(
                queries.getQuery("get-max-score-in-category.sql"),
                new MapSqlParameterSource(GRADE_TYPE_PARAMETER, category.name()),
                Integer.class
            )
        );
    }

    @Override
    public Optional<Integer> minScore(GradeType category) {
        return Optional.ofNullable(jdbcTemplate
            .queryForObject(
                queries.getQuery("get-min-score-in-category.sql"),
                new MapSqlParameterSource(GRADE_TYPE_PARAMETER, category.name()),
                Integer.class
            )
        );
    }

    @Override
    public Optional<Integer> medianScore(GradeType category) {
        try {
            return Optional.ofNullable(jdbcTemplate
                .queryForObject(
                    queries.getQuery("get-median-score-in-category.sql"),
                    new MapSqlParameterSource(GRADE_TYPE_PARAMETER, category.name()),
                    Integer.class
                )
            );
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }
}