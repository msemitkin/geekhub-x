package org.geekhub.studentsregistry.reader;

import org.geekhub.studentsregistry.grade.CommonGradeFactory;
import org.geekhub.studentsregistry.grade.GradeAdapter;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.logger.Logging;
import org.geekhub.studentsregistry.student.Student;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class AutoStudentsGenerator implements StudentsReader {
    private final List<String> mostPopularNames = List.of(
        "James", "Mary", "Patricia", "Robert", "Jennifer", "Michael", "Linda", "William",
        "Elizabeth", "David", "Barbara", "Richard", "Susan", "Joseph", "Jessica", "Thomas",
        "Sarah", "Charles", "Karen", "Christopher", "Nancy", "Daniel", "Lisa", "Matthew",
        "Margaret", "Anthony", "Betty", "Donald", "Sandra", "Mark", "Ashley", "Paul",
        "Dorothy", "Steven", "Kimberly", "Andrew", "Emily", "Kenneth", "Donna"
    );
    private final Random randomGenerator;

    public AutoStudentsGenerator(Random randomGenerator) {
        this.randomGenerator = randomGenerator;
    }

    @Override
    public List<Student> readStudents(int count) {
        if (count < 1) {
            throw new IllegalArgumentException();
        }
        Logging.getLogger()
            .info(String.format("Group exists of %d students. Generating their data...%n", count));
        List<Student> generatedStudents = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            generatedStudents.add(generateStudent());
        }
        Logging.getLogger().info("Students have been generated%n%n");
        return generatedStudents;
    }

    private Student generateStudent() {
        return new Student(generateName(), generateGrade(), generateExamDate(), generateExamTime());
    }

    private String generateName() {
        return mostPopularNames.get(randomGenerator.nextInt(mostPopularNames.size()));
    }

    private GradeAdapter generateGrade() {
        CommonGradeFactory commonGradeFactory = new CommonGradeFactory();
        return commonGradeFactory.createGrade(generateGradeType(), generateGradeValue());
    }

    private int generateGradeValue() {
        return switch (randomGenerator.nextInt(5)) {
            case 0 -> randomGenerator.nextInt(60);
            case 1 -> 60 + randomGenerator.nextInt(10);
            case 2 -> 70 + randomGenerator.nextInt(10);
            case 3 -> 80 + randomGenerator.nextInt(10);
            case 4 -> 90 + randomGenerator.nextInt(11);
            default -> throw new IllegalStateException("Unexpected value");
        };
    }

    private GradeType generateGradeType() {
        return GradeType.values()[randomGenerator.nextInt(GradeType.values().length)];
    }

    private LocalDate generateExamDate() {
        return LocalDate.of(2019, Month.MAY, 1 + randomGenerator.nextInt(31));
    }

    private LocalTime generateExamTime() {
        return LocalTime.of(8 + randomGenerator.nextInt(9), randomGenerator.nextInt(60));
    }
}
