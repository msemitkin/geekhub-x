package org.geekhub.studentsregistry.io;

import com.opencsv.bean.CsvBindByName;

public class CSVStudent {

    @CsvBindByName(column = "name")
    private String name;
    @CsvBindByName(column = "score")
    private Integer score;
    @CsvBindByName(column = "grade-type")
    private String gradeType;
    @CsvBindByName(column = "date")
    private String date;
    @CsvBindByName(column = "time")
    private String time;

    public CSVStudent() {
    }

    public CSVStudent(String name, Integer score, String gradeType, String date, String time) {
        this.name = name;
        this.score = score;
        this.gradeType = gradeType;
        this.date = date;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getGradeType() {
        return gradeType;
    }

    public void setGradeType(String gradeType) {
        this.gradeType = gradeType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
