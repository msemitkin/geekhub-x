package org.geekhub.studentsregistry.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;

public class FileLogger implements Logger {
    private static final Path LOG_FILE_PATH =
        Path.of(System.getProperty("user.home")).resolve("log.txt");


    @Override
    public void info(String message) {
        printToFile(message);
    }

    @Override
    public void error(String message) {
        printToFile("[ERROR] " + message);
    }

    private void printToFile(String message) {
        try (FileWriter fileWriter = new FileWriter(LOG_FILE_PATH.toString(), true);
             PrintWriter writer = new PrintWriter(fileWriter)) {
            writer.println(message);
        } catch (IOException e) {
            System.out.println("Cannot write log to file");
        }
    }
}
