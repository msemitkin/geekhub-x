package org.geekhub.studentsregistry.grade;

public class LetterGrade extends AmericanGrade {

    public LetterGrade(int intGrade) {
        value = intGrade;
    }

    @Override
    public String asPrintVersion() {
        if (0 <= value && value < 60) {
            return "F";
        } else if (60 <= value && value < 70) {
            return "D";
        } else if (70 <= value && value < 80) {
            return "C";
        } else if (80 <= value && value < 90) {
            return "B";
        } else if (90 <= value && value <= 100) {
            return "A";
        } else {
            throw new InvalidScoreException();
        }
    }
}
