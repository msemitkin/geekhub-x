package org.geekhub.studentsregistry.cache;

import org.geekhub.studentsregistry.analyst.Analytics;
import org.geekhub.studentsregistry.grade.GradeType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Configuration
public class CacheConfig {

    @Bean
    public Cache<GradeType, Analytics> analyticsCache() {
        ScheduledExecutorService executorService =
            Executors.newSingleThreadScheduledExecutor();
        Integer invalidationTimeout = 5;
        TimeUnit timeUnit = TimeUnit.MINUTES;
        return new Cache<>(executorService, invalidationTimeout, timeUnit);
    }
}
