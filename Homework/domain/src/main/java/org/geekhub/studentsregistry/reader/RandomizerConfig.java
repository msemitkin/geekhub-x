package org.geekhub.studentsregistry.reader;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
public class RandomizerConfig {
    @Bean
    public Random random() {
        return new Random();
    }
}
