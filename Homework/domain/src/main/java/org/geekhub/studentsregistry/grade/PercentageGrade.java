package org.geekhub.studentsregistry.grade;

public class PercentageGrade extends AmericanGrade {
    public PercentageGrade(int grade) {
        this.value = grade;
    }

    @Override
    public String asPrintVersion() {
        if (0 <= value && value < 60) {
            return "< 60%";
        } else if (60 <= value && value < 70) {
            return "60 - 69%";
        } else if (70 <= value && value < 80) {
            return "70 - 79%";
        } else if (80 <= value && value < 90) {
            return "80 - 89%";
        } else if (90 <= value && value <= 100) {
            return "90 - 100%";
        } else {
            throw new InvalidScoreException();
        }
    }
}
