package org.geekhub.studentsregistry.analyst;

import org.geekhub.studentsregistry.grade.GradeType;

import java.util.Map;

public interface StudentAnalystOverall {
    Map<GradeType, Analytics> getAnalytics();
}
