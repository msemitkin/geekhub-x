package org.geekhub.studentsregistry.analyst;

import org.geekhub.studentsregistry.grade.GradeType;

import java.util.Optional;

public interface StudentsAnalyst {

    Optional<Integer> maxScore(GradeType category);

    Optional<Integer> minScore(GradeType category);

    Optional<Integer> medianScore(GradeType category);
}