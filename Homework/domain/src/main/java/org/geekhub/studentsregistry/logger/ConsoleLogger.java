package org.geekhub.studentsregistry.logger;

public class ConsoleLogger implements Logger {
    @Override
    public void info(String message) {
        System.out.println(message);
    }

    @Override
    public void error(String message) {
        System.err.println("[ERROR] " + message);
    }
}
