package org.geekhub.studentsregistry.storage.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.output.MigrateResult;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
@ComponentScan("org.geekhub.studentsregistry")
@PropertySource("classpath:database.properties")
public class DatabaseConfig {

    @Bean("dataSource")
    @Conditional(H2DatabaseEnabled.class)
    public DataSource h2DataSource(
        @Value("${h2.url}") String url,
        @Value("${h2.username}") String username,
        @Value("${h2.password}") String password,
        @Value("${h2.driver}") String driverClassName
    ) {
        return hikariDataSource(url, username, password, driverClassName);
    }

    @Bean("dataSource")
    @Conditional(PostgresDatabaseEnabled.class)
    public DataSource postgresDataSource(
        @Value("${postgres.url}") String url,
        @Value("${postgres.username}") String username,
        @Value("${postgres.password}") String password,
        @Value("${postgres.driver}") String driverClassName
    ) {
        return hikariDataSource(url, username, password, driverClassName);
    }

    private DataSource hikariDataSource(
        String url,
        String username,
        String password,
        String driverClassName
    ) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);
        hikariConfig.setDriverClassName(driverClassName);
        hikariConfig.setPoolName("StudentsRegistryApp");
        hikariConfig.setMaximumPoolSize(10);
        hikariConfig.setConnectionTimeout(1000);
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public NamedParameterJdbcTemplate h2JdbcTemplate(
        @Qualifier("dataSource") DataSource dataSource
    ) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    public MigrateResult migrateDatabase(
        @Qualifier("dataSource") DataSource dataSource,
        @Value("classpath:db/migration") String location
    ) {
        return Flyway.configure()
            .locations(location)
            .dataSource(dataSource)
            .load()
            .migrate();
    }

}