package org.geekhub.studentsregistry.grade;

import org.geekhub.studentsregistry.InvalidInputException;

public class ParameterNotSpecifiedException extends InvalidInputException {
    public ParameterNotSpecifiedException() {
    }
}
