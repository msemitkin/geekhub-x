package org.geekhub.studentsregistry.mapper;

import org.geekhub.studentsregistry.grade.CommonGradeFactory;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.storage.database.StudentEntity;
import org.geekhub.studentsregistry.student.Student;
import org.springframework.stereotype.Service;

@Service
public class StudentMapper {

    private final CommonGradeFactory gradeFactory;

    public StudentMapper(CommonGradeFactory gradeFactory) {
        this.gradeFactory = gradeFactory;
    }

    public StudentEntity toEntity(Student student) {
        Integer score = student.grade().getGrade();
        GradeType gradeType = GradeType.resolveGradeType(student.grade());
        return new StudentEntity(
            null,
            student.name(),
            score,
            gradeType,
            student.examDate(),
            student.examTime()
        );
    }

    public Student toStudent(StudentEntity studentEntity) {
        GradeType gradeType = studentEntity.getGradeType();
        Integer score = studentEntity.getScore();
        return new Student(
            studentEntity.getName(),
            gradeFactory.createGrade(gradeType, score),
            studentEntity.getExamDate(),
            studentEntity.getExamTime()
        );
    }


}
