package org.geekhub.studentsregistry.storage;

import org.geekhub.studentsregistry.analyst.Analytics;
import org.geekhub.studentsregistry.cache.Cache;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.storage.database.StudentEntity;
import org.geekhub.studentsregistry.storage.database.StudentRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentsStorageManager {

    private final Cache<GradeType, Analytics> cache;
    private final StudentRepository studentRepository;

    public StudentsStorageManager(
        Cache<GradeType, Analytics> cache,
        StudentRepository studentRepository
    ) {
        this.cache = cache;
        this.studentRepository = studentRepository;
    }

    public List<StudentEntity> readStudents() {
        return studentRepository.findAll();
    }

    public void save(StudentEntity student) {
        cache.invalidate();
        studentRepository.save(student);
    }

    public void writeStudents(List<StudentEntity> students) {
        cache.invalidate();
        studentRepository.saveAll(students);
    }

    public void deleteAll() {
        cache.invalidate();
        studentRepository.deleteAll();
    }

    public void deleteById(int id) {
        cache.invalidate();
        studentRepository.deleteById(id);
    }

    public boolean existsWithNameIgnoreCase(String name) {
        return studentRepository.existsStudentEntityByNameIgnoreCase(name);
    }

    public boolean existsById(Integer id) {
        return studentRepository.existsById(id);
    }

}
