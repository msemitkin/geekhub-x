package org.geekhub.studentsregistry.grade;

public abstract class AmericanGrade extends GradeAdapter {

    @Override
    public int compareTo(GradeAdapter anotherGrade) {
        int[][] intervals = new int[][]{{0, 59}, {60, 69}, {70, 79}, {80, 89}, {90, 100}};
        for (int[] interval : intervals) {
            if (interval[0] <= value
                && value <= interval[1]
                && interval[0] <= anotherGrade.getGrade()
                && anotherGrade.getGrade() <= interval[1]) {
                return 0;
            }
        }
        return value - anotherGrade.getGrade();
    }
}
