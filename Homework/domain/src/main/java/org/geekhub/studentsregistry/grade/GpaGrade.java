package org.geekhub.studentsregistry.grade;

public class GpaGrade extends AmericanGrade {

    public GpaGrade(int intGrade) {
        value = intGrade;
    }

    @Override
    public String asPrintVersion() {
        if (0 <= value && value < 60) {
            return "0.0";
        } else if (60 <= value && value < 70) {
            return "1.0";
        } else if (70 <= value && value < 80) {
            return "2.0";
        } else if (80 <= value && value < 90) {
            return "3.0";
        } else if (90 <= value && value <= 100) {
            return "4.0";
        } else {
            throw new InvalidScoreException();
        }
    }
}
