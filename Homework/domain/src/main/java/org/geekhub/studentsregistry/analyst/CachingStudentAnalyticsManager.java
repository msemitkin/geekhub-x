package org.geekhub.studentsregistry.analyst;

import org.geekhub.studentsregistry.cache.Cache;
import org.geekhub.studentsregistry.grade.GradeType;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CachingStudentAnalyticsManager {

    private final Cache<GradeType, Analytics> cache;
    private final StudentAnalystOverall studentAnalystOverall;

    public CachingStudentAnalyticsManager(
        Cache<GradeType, Analytics> cache,
        StudentAnalystOverall studentAnalystOverall
    ) {
        this.cache = cache;
        this.studentAnalystOverall = studentAnalystOverall;
    }

    public Map<GradeType, Analytics> getAnalytics() {
        Map<GradeType, Analytics> analytics = cache.getAll();
        if (!analytics.isEmpty()) {
            return analytics;
        }
        Map<GradeType, Analytics> computedAnalytics = studentAnalystOverall.getAnalytics();
        cache.update(computedAnalytics);
        return computedAnalytics;
    }

}
