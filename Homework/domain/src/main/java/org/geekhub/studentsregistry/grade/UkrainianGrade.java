package org.geekhub.studentsregistry.grade;

public class UkrainianGrade extends GradeAdapter {
    public UkrainianGrade(int value) {
        this.value = value;
    }

    @Override
    public String asPrintVersion() {
        if (0 <= value && value < 5) {
            return "1";
        } else if (5 <= value && value < 10) {
            return "2";
        } else if (10 <= value && value < 20) {
            return "3";
        } else if (20 <= value && value < 30) {
            return "4";
        } else if (30 <= value && value < 40) {
            return "5";
        } else if (40 <= value && value < 50) {
            return "6";
        } else if (50 <= value && value < 60) {
            return "7";
        } else if (60 <= value && value < 70) {
            return "8";
        } else if (70 <= value && value < 80) {
            return "9";
        } else if (80 <= value && value < 90) {
            return "10";
        } else if (90 <= value && value < 100) {
            return "11";
        } else if (value == 100) {
            return "12";
        } else {
            throw new InvalidScoreException();
        }
    }

    @Override
    public int compareTo(GradeAdapter anotherGrade) {
        int[][] intervals = new int[][]{{0, 4}, {5, 9}, {10, 19}, {20, 29}, {30, 39},
            {40, 49}, {50, 59}, {60, 69}, {70, 79}, {80, 89}, {90, 99}, {100, 100}};
        for (int[] interval : intervals) {
            if (interval[0] <= value
                && value <= interval[1]
                && interval[0] <= anotherGrade.getGrade()
                && anotherGrade.getGrade() <= interval[1]) {
                return 0;
            }
        }
        return value - anotherGrade.getGrade();
    }
}
