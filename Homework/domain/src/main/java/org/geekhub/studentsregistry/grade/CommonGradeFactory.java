package org.geekhub.studentsregistry.grade;

import org.springframework.stereotype.Service;

@Service
public class CommonGradeFactory implements GradeFactory {

    @Override
    public GradeAdapter createGrade(GradeType gradeType, int value) {
        if (gradeType == null) {
            throw new IllegalArgumentException();
        }
        return switch (gradeType) {
            case GPA -> new GpaGrade(value);
            case LETTER -> new LetterGrade(value);
            case PERCENTAGE -> new PercentageGrade(value);
            case UKRAINIAN -> new UkrainianGrade(value);
        };
    }
}
