package org.geekhub.studentsregistry.student;


import org.geekhub.studentsregistry.grade.GradeAdapter;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;


public record Student(String name, GradeAdapter grade, LocalDate examDate, LocalTime examTime)
    implements Serializable, Comparable<Student> {

    @Override
    public int compareTo(Student anotherStudent) {
        int compareGrades = grade.compareTo(anotherStudent.grade);
        if (compareGrades != 0) {
            return -compareGrades;
        } else {
            return name.compareToIgnoreCase(anotherStudent.name);
        }
    }
}
