package org.geekhub.studentsregistry.storage.database;

import org.geekhub.studentsregistry.grade.GradeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "student")
public class StudentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Integer score;
    @Enumerated(EnumType.STRING)
    private GradeType gradeType;
    @Column(name = "date")
    private LocalDate examDate;
    @Column(name = "time")
    private LocalTime examTime;

    protected StudentEntity() {
    }

    public StudentEntity(
        Integer id,
        String name,
        Integer score,
        GradeType gradeType,
        LocalDate examDate,
        LocalTime examTime
    ) {
        this.id = id;
        this.name = name;
        this.score = score;
        this.gradeType = gradeType;
        this.examDate = examDate;
        this.examTime = examTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public GradeType getGradeType() {
        return gradeType;
    }

    public void setGradeType(GradeType gradeType) {
        this.gradeType = gradeType;
    }

    public LocalDate getExamDate() {
        return examDate;
    }

    public void setExamDate(LocalDate examDate) {
        this.examDate = examDate;
    }

    public LocalTime getExamTime() {
        return examTime;
    }

    public void setExamTime(LocalTime examTime) {
        this.examTime = examTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentEntity that = (StudentEntity) o;
        return Objects.equals(id, that.id) &&
               Objects.equals(name, that.name) &&
               Objects.equals(score, that.score) &&
               gradeType == that.gradeType &&
               Objects.equals(examDate, that.examDate) &&
               Objects.equals(examTime, that.examTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, score, gradeType, examDate, examTime);
    }

    @Override
    public String toString() {
        return "StudentEntity{" +
               "id=" + id +
               ", name='" + name + '\'' +
               ", score=" + score +
               ", gradeType=" + gradeType +
               ", examDate=" + examDate +
               ", examTime=" + examTime +
               '}';
    }
}
