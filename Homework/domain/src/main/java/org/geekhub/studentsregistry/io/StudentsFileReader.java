package org.geekhub.studentsregistry.io;

import org.geekhub.studentsregistry.student.Student;

import java.util.List;

public interface StudentsFileReader {
    List<Student> getStudents();
}
