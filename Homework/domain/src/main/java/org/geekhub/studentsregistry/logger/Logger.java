package org.geekhub.studentsregistry.logger;

public interface Logger {
    void info(String message);

    void error(String message);
}
