package org.geekhub.studentsregistry.analyst;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.geekhub.studentsregistry.grade.GradeType;
import org.geekhub.studentsregistry.logger.Logger;
import org.geekhub.studentsregistry.logger.Logging;
import org.springframework.stereotype.Service;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
public class StudentAnalystOverallImpl implements StudentAnalystOverall {
    private static final Logger logger = Logging.getLogger();

    private final StudentsAnalyst studentsAnalyst;

    public StudentAnalystOverallImpl(StudentsAnalyst studentsAnalyst) {
        this.studentsAnalyst = studentsAnalyst;
    }

    @Override
    public Map<GradeType, Analytics> getAnalytics() {
        Map<GradeType, Analytics> analyticsMap = new EnumMap<>(GradeType.class);
        for (GradeType gradeType : GradeType.values()) {
            analyticsMap.put(gradeType, getAnalyticsInCategory(gradeType));
        }
        return analyticsMap;
    }

    private Analytics getAnalyticsInCategory(GradeType gradeType) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        try {
            Future<Optional<Integer>> maxScore = executorService.submit(
                () -> studentsAnalyst.maxScore(gradeType));
            Future<Optional<Integer>> minScore = executorService.submit(
                () -> studentsAnalyst.minScore(gradeType));
            Future<Optional<Integer>> medianScore = executorService.submit(
                () -> studentsAnalyst.medianScore(gradeType));
            return new Analytics(
                getFutureResultSafely(maxScore),
                getFutureResultSafely(minScore),
                getFutureResultSafely(medianScore)
            );
        } finally {
            executorService.shutdown();
        }
    }

    private Optional<Integer> getFutureResultSafely(Future<Optional<Integer>> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            if (e instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            }
            String message = String.format(
                "Failed to compute analytics%n%s",
                ExceptionUtils.getStackTrace(e)
            );
            logger.error(message);
            throw new ComputationException(e);
        }
    }
}
