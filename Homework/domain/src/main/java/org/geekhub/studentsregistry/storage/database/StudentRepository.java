package org.geekhub.studentsregistry.storage.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Integer> {

    boolean existsStudentEntityByNameIgnoreCase(String name);
}
