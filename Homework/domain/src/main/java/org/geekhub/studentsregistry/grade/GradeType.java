package org.geekhub.studentsregistry.grade;

import java.util.Objects;

public enum GradeType {
    UKRAINIAN, LETTER, PERCENTAGE, GPA;

    public static GradeType from(String inputGradeType) {
        if (Objects.isNull(inputGradeType)) {
            throw new IllegalArgumentException();
        }
        if (inputGradeType.isBlank()) {
            throw new ParameterNotSpecifiedException();
        }
        for (GradeType gradeType : GradeType.values()) {
            if (gradeType.name().equalsIgnoreCase(inputGradeType)) {
                return gradeType;
            }
        }
        throw new GradeNotSupportedException();
    }

    public static GradeType resolveGradeType(Grade grade) {
        if (grade instanceof LetterGrade) {
            return GradeType.LETTER;
        } else if (grade instanceof PercentageGrade) {
            return GradeType.PERCENTAGE;
        } else if (grade instanceof GpaGrade) {
            return GradeType.GPA;
        } else if (grade instanceof UkrainianGrade) {
            return GradeType.UKRAINIAN;
        } else {
            throw new GradeNotSupportedException();
        }
    }
}


