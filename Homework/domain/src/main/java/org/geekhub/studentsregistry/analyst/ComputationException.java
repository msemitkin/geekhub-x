package org.geekhub.studentsregistry.analyst;

public class ComputationException extends RuntimeException {
    public ComputationException(Throwable cause) {
        super(cause);
    }
}
