package org.geekhub.studentsregistry.analyst;

import java.util.Objects;
import java.util.Optional;

public class Analytics {

    private final Optional<Integer> max;
    private final Optional<Integer> min;
    private final Optional<Integer> median;


    public Analytics(
        Optional<Integer> max,
        Optional<Integer> min,
        Optional<Integer> median
    ) {
        this.max = max;
        this.min = min;
        this.median = median;
    }

    public Optional<Integer> getMax() {
        return max;
    }

    public Optional<Integer> getMin() {
        return min;
    }

    public Optional<Integer> getMedian() {
        return median;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Analytics analytics = (Analytics) o;
        return Objects.equals(max, analytics.max) &&
               Objects.equals(min, analytics.min) &&
               Objects.equals(median, analytics.median);
    }

    @Override
    public int hashCode() {
        return Objects.hash(max, min, median);
    }
}
