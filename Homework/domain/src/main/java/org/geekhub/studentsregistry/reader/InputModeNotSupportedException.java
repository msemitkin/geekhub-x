package org.geekhub.studentsregistry.reader;

import org.geekhub.studentsregistry.InvalidInputException;

public class InputModeNotSupportedException extends InvalidInputException {
    InputModeNotSupportedException() {

    }
}
