# GeekHub X

Java for Web project for GeekHub, Season X

Swagger ui is available on:
http://localhost:8080/swagger-ui/

Polling students from file  
- file should be placed in user home directory
- required .csv format
- first line should be header, so it'll be skipped while parsing
- Data order matters and should be as follows:
  `name,score,grade-type,date,time`


## My Course Project:
https://gitlab.com/msemitkin/doctors-registry
